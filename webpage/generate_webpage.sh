#! /bin/bash

# Université d'Aix Marseille (AMU) -
# Centre National de la Recherche Scientifique (CNRS) -
# Université de Toulon (UT).
# Copyright © 2016-2018 AMU, CNRS, UT
#
# This file is part of MinCoverPetri.
#
# MinCoverPetri is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MinCoverPetri is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Florent JAILLET - Laboratoire d'Informatique et Systèmes - UMR 7020


# This script generates the main webpage for the MinCoverPetri project using
# the file readme.md of the project and some information taken from the
# generated application.
#
# Usage:
# ./generate_webpage.sh <readme_filename> <bin_dir> <out_filename>
#
# <readme_filename>   Filename (full path) of the readme file of the
#                     MinCoverPetri project
# <mincoverpetri_exe> Full path of the MinCoverPetri executable
# <out_filename>      Filename (full path) of the output webpage

README_FILENAME=$1
MINCOVERPETRI_EXE=$2
OUT_FILENAME=$3

TMP_PREFIX=/tmp/mincoverpetri_tmp_file_

VERSION=$(${MINCOVERPETRI_EXE} -v)

echo \
'<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>MinCoverPetri</title>
</head>
<body>' > ${TMP_PREFIX}htmlstart

echo \
'</body>
</html>' > ${TMP_PREFIX}htmlend

echo \
"

" > ${TMP_PREFIX}example

echo \
"
Download
----

### Precompiled executables

Here are the precompiled executables for MinCoverpetri version $VERSION:

- GNU/Linux x86-64:
  [mincoverpetri-$VERSION-Linux64.zip](mincoverpetri-$VERSION-Linux64.zip)
- Windows 32 bits:
  [mincoverpetri-$VERSION-Windows32.zip](mincoverpetri-$VERSION-Windows32.zip)
- Windows 64 bits:
  [mincoverpetri-$VERSION-Windows64.zip](mincoverpetri-$VERSION-Windows64.zip)

### Source code

The source code can be downloaded from the associated
[GitLab project](https://gitlab.lis-lab.fr/dev/mincoverpetri).

The build instructions are given in the
[developer
documentation](http://dev.pages.lis-lab.fr/mincoverpetri/doc/src/html).

### Example data

The following sample PNML file representing the Petri net of Figure 7 in the
reference article cited [above](#article) is available:
[figure_7.pnml](figure_7.pnml).

To test MinCoverPetri using this data, simply run:

    ./mincoverpetri figure_7.pnml

" > ${TMP_PREFIX}download

echo \
"
Documentation
----

### User documentation

The help of the application is available from the command line using the \`-h\`
option, simply run:

    ./mincoverpetri -h
    
which gives:

<pre>" > ${TMP_PREFIX}doc

${MINCOVERPETRI_EXE} -h >> ${TMP_PREFIX}doc

echo \
"
</pre>

### Developer documentation

The developer documentation is available
[here](http://dev.pages.lis-lab.fr/mincoverpetri/doc/src/html).

" >> ${TMP_PREFIX}doc

csplit --silent --prefix ${TMP_PREFIX} ${README_FILENAME} \
    "/^Download$/" "/^Documentation$/" "/^Dependencies$/"
cat ${TMP_PREFIX}00 ${TMP_PREFIX}download ${TMP_PREFIX}doc ${TMP_PREFIX}03 \
    > ${TMP_PREFIX}full
markdown ${TMP_PREFIX}full > ${TMP_PREFIX}htmlbody
cat ${TMP_PREFIX}htmlstart ${TMP_PREFIX}htmlbody ${TMP_PREFIX}htmlend \
    > ${OUT_FILENAME}
rm ${TMP_PREFIX}*
