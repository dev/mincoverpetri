<!--
Université d'Aix Marseille (AMU) -
Centre National de la Recherche Scientifique (CNRS) -
Université de Toulon (UT).
Copyright © 2016-2018 AMU, CNRS, UT

This file is part of MinCoverPetri.

MinCoverPetri is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MinCoverPetri is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.

Author: Florent JAILLET - Laboratoire d'Informatique et Systèmes - UMR 7020
-->

MinCoverPetri
====

[MinCoverPetri](http://dev.pages.lis-lab.fr/mincoverpetri) is a
command-line application for the fast computation of the minimal coverability
set of place/transition (P/T) Petri nets.

It can process P/T Petri nets stored in the Petri Net Markup Language (PNML)
file format defined by the standard ISO/IEC 15909 Part 2.  
See [pnml.org](http://www.pnml.org) for details about this format.

<a name="article"></a> It uses the monotone pruning algorithm proposed in the
[article](http://pageperso.lif.univ-mrs.fr/~pierre-alain.reynier/publis/fi13.pdf):

 > Minimal Coverability Set for Petri Nets: Karp and Miller Algorithm with
 > Pruning.  
 > P.-A. Reynier and F. Servais.  
 > In Fundamenta Informaticae, vol. 122, no. 1-2, pp. 1-30, IOS Press,2013.  
 > DOI: [10.3233/FI-2013-781](https://doi.org/10.3233/FI-2013-781)

MinCoverPetri is developed in C++ by the
[development team](http://developpement.lif.univ-mrs.fr) of the
[LIS](http://www.lis-lab.fr/).

License
----

MinCoverPetri is free software released under the terms of the [GNU General
Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) or
later (GPLv3+).

Download
----

**Precompiled executables** for GNU/Linux and Windows can be downloaded from
the [MinCoverPetri website](http://dev.pages.lis-lab.fr/mincoverpetri).

The **source code** can be downloaded from the associated
[GitLab project](https://gitlab.lis-lab.fr/dev/mincoverpetri).

The build instructions are given in the
[developer
documentation](http://dev.pages.lis-lab.fr/mincoverpetri/doc/src/html)
and in the file
[src/developer_documentation.dox](src/developer_documentation.dox).

Documentation
----

### User documentation

The help of the application is available from the command line using the `-h`
option, simply run:

    mincoverpetri -h

This help is also replicated on the
[MinCoverPetri website](http://dev.pages.lis-lab.fr/mincoverpetri).

### Developer documentation

The [developer
documentation](http://dev.pages.lis-lab.fr/mincoverpetri/doc/src/html)
is available online.

It can also be automatically generated from the source code using
[Doxygen](http://www.doxygen.org) (see
[src/developer_documentation.dox](src/developer_documentation.dox)
for details).

Dependencies
----

The code of MinCoverPetri uses the following third party libraries:

 * [args](https://github.com/Taywee/args), released under an MIT license,
 * [TinyXML-2](http://www.grinninglizard.com/tinyxml2), released under the
   ZLib license.

The code of the tests uses the [Catch](https://github.com/philsquared/Catch)
framework released under the Boost Software License 1.0.

Contact
----

Issues can be reported in the
[GitLab project](https://gitlab.lis-lab.fr/dev/mincoverpetri) or by email
to `contact.dev`@`lis-lab.fr`.

