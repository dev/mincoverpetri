/**
 * \file
 * \brief Tools to represent and manipulate omega-markings
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the elements used to represent and manipulate
 * omega-markings of Petri nets.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "marking.hpp"

#include <cassert>
#include <utility>


// For debugging purpose, the flag TRACE_MARKINGS can be defined to trace the
// construction, destruction, move and copy of omega-markings, using:
// #define TRACE_MARKINGS

Marking::Marking(std::size_t count, NbTokens value) :
    values_(count, value) {
#ifdef TRACE_MARKINGS
  std::cout << "Construct " << __FUNCTION__ << " " << this
      << " from count and value" << std::endl;
#endif
}

Marking::Marking(std::initializer_list<NbTokens> init) :
    values_(init) {
#ifdef TRACE_MARKINGS
  std::cout << "Construct " << __FUNCTION__ << " " << this
      << " from init list" << std::endl;
#endif
}

Marking::Marking(const Marking& marking) :
    values_(marking.values_) {
#ifdef TRACE_MARKINGS
  std::cout << "Copy " << __FUNCTION__ << " " << &marking << " to " << this
      << std::endl;
#endif
}

Marking::Marking(Marking&& marking) :
    values_(std::move(marking.values_)) {
#ifdef TRACE_MARKINGS
  std::cout << "Move " << __FUNCTION__ << " " << &marking << " to " << this
      << std::endl;
#endif
}

Marking::~Marking() {
#ifdef TRACE_MARKINGS
  std::cout << "Destruct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

bool Marking::operator==(const Marking& marking_rhs) const {
  assert(values_.size() == marking_rhs.values_.size());
  return !(*this != marking_rhs);
}

bool Marking::operator!=(const Marking& marking_rhs) const {
  assert(values_.size() == marking_rhs.values_.size());
  for (std::size_t ind = 0, size = values_.size(); ind < size; ++ind) {
    if (values_[ind] != marking_rhs.values_[ind]) {
      return true;
    }
  }
  return false;
}

bool Marking::operator<=(const Marking& marking_rhs) const {
  assert(values_.size() == marking_rhs.values_.size());
  for (std::size_t ind = 0, size = values_.size(); ind < size; ++ind) {
    if (values_[ind] > marking_rhs.values_[ind]) {
      return false;
    }
  }
  return true;
}

bool Marking::operator<(const Marking& marking_rhs) const {
  assert(values_.size() == marking_rhs.values_.size());
  return (*this <= marking_rhs) && (*this != marking_rhs);
}


void Marking::add_omegaless_marking(const Marking& omegaless_marking) {
  assert(values_.size() == omegaless_marking.values_.size());
  std::size_t size = values_.size();
  for (std::size_t ind = 0; ind < size; ++ind) {
    if (values_[ind] != omega) {
      values_[ind] += omegaless_marking.values_[ind];
    }
  }
}

void Marking::accelerate_helper(const Marking& marking) {
  assert(values_.size() == marking.values_.size());
  for (std::size_t ind = 0, size = values_.size(); ind < size; ++ind) {
    if (marking.values_[ind] < values_[ind]) {
      values_[ind] = omega;
    }
  }
}

void Marking::print_value(std::size_t index, std::ostream& os) const {
  if (values_[index] == Marking::omega) {
    os << "w";
  } else {
    os << static_cast<signed>(values_[index]);
  }
}

std::ostream& operator<<(std::ostream& os, const Marking& marking) {
  if (marking.size() == 0) {
    return os;
  }
  for (std::size_t ind = 0, lim = marking.values_.size()-1; ind < lim; ++ind) {
    marking.print_value(ind, os);
    os << " ";
  }
  marking.print_value(marking.values_.size()-1, os);
  return os;
}
