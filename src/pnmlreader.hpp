/**
 * \file
 * \brief Header for tools to read PNML Petri net files
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the tools to read place/transition (P/T) Petri net files
 * in the Petri Net Markup Language (PNML) file format defined by the standard
 * ISO/IEC 15909 Part 2.
 *
 * See http://www.pnml.org/ for details about the PNML file format.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PNMLREADER_HPP_
#define PNMLREADER_HPP_

#include <string>

#include "../third_party/tinyxml2.h"

#include "petrinet.hpp"


/**
 * \brief PNML place/transition Petri net file reader
 *
 * This class reads place/transition (P/T) Petri net files in the Petri Net
 * Markup Language (PNML) file format defined by the standard ISO/IEC 15909
 * Part 2.
 *
 * See http://www.pnml.org/ for details about the PNML file format.
 *
 * \note This class uses the TinyXML-2 library to parse the PNML file.
 *       More details can be found here:
 *       http://www.grinninglizard.com/tinyxml2.
 */
class PnmlReader {
public:
  /**
   * \brief Reads a PNML place/transition Petri net file
   *
   * This function reads a place/transition (P/T) Petri net file in the Petri
   * Net Markup Language (PNML) file format defined by the standard ISO/IEC
   * 15909 Part 2.
   *
   * See http://www.pnml.org/ for details about the PNML file format.
   *
   * \param filename Filename of the input PNML file (extension .pnml).
   *
   * \param omega_value When reading the input PNML file, if an initial marking
   *        value in a place is greater than or equal to \p omega_value it is
   *        converted in the output Petri net to the value Marking::omega.
   *
   * \return Output Petri net.
   */
  PetriNet read(const std::string& filename, long long int omega_value = 127);

private:
  /**
   * \brief Pointer to the page XML element of the PNML file which contains the
   *        Petri net description
   */
  tinyxml2::XMLElement* page_elem_;

  /**
   * \brief Reads the places in the file
   *
   * \param omega_value When reading the input PNML file, if an initial marking
   *        value in a place is greater than or equal to \p omega_value it is
   *        converted in the output Petri net to the value Marking::omega.
   * \param net Petri net in which the new read places must be added.
   */
  void read_places(long long int omega_value, PetriNet& net) const;
  /**
   * \brief Reads the transitions in the file
   *
   * \param net Petri net in which the new read transitions must be added.
   */
  void read_transitions(PetriNet& net) const;
  /**
   * \brief Reads the arcs in the file
   *
   * \param net Petri net in which the new read arcs must be added.
   */
  void read_arcs(PetriNet& net) const;
};

#endif /* PNMLREADER_HPP_ */
