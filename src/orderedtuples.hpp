/**
 * \file
 * \brief Header for tools to represent and manipulate ordered tuples used to
 *        accelerate key steps of the monotone pruning algorithm
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares a data structure representing ordered tuples. It provides
 * associated functionalities specifically designed to accelerate key steps of
 * the monotone pruning algorithm.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ORDEREDTUPLES_HPP_
#define ORDEREDTUPLES_HPP_

#include <vector>

#include "marking.hpp"
#include "node.hpp"


/**
 * \brief Data structure storing and manipulating ordered tuples designed to
 *        accelerate key steps of the monotone pruning algorithm
 *
 * This class represents a set of tuples using a hierarchical data structure
 * that keeps the stored tuples ordered.
 *
 * The representation consists of nodes, each node storing the value of one
 * component of a tuple.
 *
 * Each node can store pointers to its first and last children, as well as to
 * its previous and next siblings. Note that depending on where the node is
 * placed in the data structure not all these pointers are used, being kept to
 * the null pointer when not useful.
 *
 * To store tuples with N components, the data structures uses N+1 levels,
 * where the level 0 corresponds to the root node of the data structure, and the
 * level L corresponds to the component of index L-1 of the tuple for 1<=L<=N.
 *
 * The nodes in the last level also store pointers to the nodes in the
 * reachability tree from the monotone pruning algorithm in order to keep track
 * of the nodes that have an omega-marking equal to the tuple stored in the
 * corresponding path of the data structure.
 *
 * The following example illustrates how the structure works.
 *
 * We consider a Petri net with 2 places and we suppose that the monotone
 * pruning algorithm produces 9 nodes in the reachability tree when processing
 * the Petri net.
 *
 * We name these nodes n0 to n8 and suppose that the associated omega-markings
 * are :
 *
 * n0~{0,3}, n1~{0,0}, n2~{4,0}, n3~{0,0}, n4~{4,1}, n5~{2,1}, n6~{4,1},
 * n7~{4,5}, n8~{4,1}.
 *
 * After inserting all these 9 nodes, the data structure stores the tuples
 * {0,0}, {0,3}, {2,1}, {4,0}, {4,1}, {4,5} and the resulting data structure can
 * be represented in the following way :
 *
 *                           root
 *                     /               \
 *                     0   =  2   =    4
 *                   /   \    /\   /       \
 *                   0 = 3    1    0 = 1 = 5
 *                   ^   ^    ^    ^   ^   ^
 *                  |n1||n0| |n5| |n2||n4||n7|
 *                  |n3|              |n6|
 *                                    |n8|
 *
 * with the following conventions:
 * * \c root is the root node,
 * * an integer value represents a node, and the value of that integer is the
 *   value of the corresponding component in the tuple,
 * * \c / represents the pointer to the first child of the node above,
 * * \c \ represents the pointer to the last child of the node above,
 * * \c = represents the pointer to the next sibling of the node on the left
 *   and the pointer to the previous sibling of the node on the right,
 * * \c ^ indicates that the node above stores pointers to the nodes in the
 *   reachability tree that are given in the column below.
 */
class OrderedTuples {
public:
  /**
   * \brief Default constructor
   *
   * This constructor creates an empty data structure.
   */
  OrderedTuples();
  /**
   * \brief Destructor
   */
  ~OrderedTuples();
  /**
   * \brief Insert a new monotone pruning node storing the associated
   *        omega-marking as an ordered tuple
   *
   * \param p_node Pointer to the new inserted node (from the monotone pruning
   *        reachability tree)
   */
  void insert(Node* p_node);
    /**
   * \brief Removes all elements from the data structure
   *
   * Removes all the ordered tuples stored in the data structure as well as the
   * associated pointers to the correspoding nodes in the  monotone pruning
   * reachability tree.
   */
  inline void clear() {
    delete_nodes();
  }

  /**
   * \brief Checks if an omega-marking value is dominated by the tuples
   *        stored in the data structure
   *
   * \param marking Tested omega-marking.
   *
   * \return true if there exists at least one active tuple in the data
   *         structure that is greater than or equal to \p marking (in the
   *         product order sense), false otherwise.
   *
   * \note This function can be used as an alternative to the function
   *       MinCoverSetComputer::is_dominated() applied to the root node of the
   *       reachability tree and leads to the exact same result.
   */
  bool is_dominated(const Marking& marking) const;
  /**
   * \brief Performs the pruning step of the monotone pruning algorithm
   *
   * This function deactivates the nodes that needs to be deactivated in the
   * reachability tree of the monotone pruning algorithm considering the value
   * of the new omega-marking processed.
   *
   * \param marking New omega-marking processed in the current iteration of the
   *        monotone pruning algorithm.
   *
   * \note
   *       * This function expects that the nodes of the reachability tree have
   *         been previously marked appropriately as being active and as being
   *         ancestors to perform the expected processing.
   *       * This function can be used as an alternative to the member function
   *         Node::prune() run on the root node of the reachability tree and
   *         leads to the exact same result.
   */
  void prune(const Marking& marking) const;
  /**
   * \brief Performs in-place acceleration of the omega-marking
   *
   * \param marking Omega-marking that must be accelerated in-place.
   *
   * \note This function can be used as an alternative to
   *       MinCoverSetComputer::accelerate(). But the two functions don't always
   *       give the same exact results as the acceleration of the omega-marking
   *       \p marking is done in-place and the order in which the values of
   *       existing markings are considered for acceleration is different in the
   *       two functions.
   *
   * \note This can result in having more acceleration (i.e. more values
   *       switched to Marking::omega in marking after execution) for one of the
   *       two %accelerate() functions.
   *
   * \note In this case, the results of both functions are anyway valid, and the
   *       monotone pruning algorithm will converge to the right solution.
   *       The over-acceleration can only in the best case lead to reducing the
   *       number of iterations needed to reach the final solution.
   */
  void accelerate(Marking& marking) const;

private:
  /**
   * \brief %Node representing the value of an element of a tuple for the tree
   *        used in OrderedTuples
   */
  struct OrderedTuplesNode {
  public:
    /**
     * Constructor
     *
     * \param input_val Value of the tuple element stored by the node.
     */
    inline explicit OrderedTuplesNode(NbTokens input_val) :
      val(input_val), p_first_child(nullptr), p_last_child(nullptr),
      p_prev_sibling(nullptr), p_next_sibling(nullptr) {
    #ifdef TRACE_ORDEREDTUPLES
      std::cout << "Construct " << __FUNCTION__ << " " << this << std::endl;
    #endif
    }
    /**
     * Destructor
     */
    inline ~OrderedTuplesNode() {
    #ifdef TRACE_ORDEREDTUPLES
      std::cout << "Destruct " << __FUNCTION__ << " " << this << std::endl;
    #endif
    }

    /**
     * \brief Insert a new monotone pruning node storing the associated
     *        omega-marking as an ordered tuple
     *
     * \param p_node Pointer to the new inserted node (from the monotone pruning
     *        reachability tree)
     *
     * \note This member function is intended to be used on the root node of the
     *       data structure to launch the recursive call that stores each
     *       component of the tuple at the appropriate level of the data
     *       structure.
     */
    void insert(Node* p_node);
    /**
     * \brief Recurses to insert a new monotone pruning node storing the
     *        selected component of the associated omega-marking in the data
     *        structure
     *
     * \param p_node Pointer to the new inserted node (from the monotone pruning
     *        reachability tree)
     * \param ind Index of the component of the omega-marking associated
     *        with the new inserted node that must be processed
     * \param p_parent Pointer to the parent node in the data structure
     */
    void insert(Node* p_node, std::size_t ind, OrderedTuplesNode* p_parent);
    /**
     * \brief Recurses to check if an omega-marking value is dominated by the
     *        tuples stored in the data structure
     *
     * \param marking Tested omega-marking.
     * \param ind Index of the omega-marking component processed.
     *
     * \return true if there exists at least one active tuple in the data
     *         structure that is greater than or equal to \p marking (in the
     *         product order sense), false otherwise.
     */
    bool is_dominated(const Marking& marking, std::size_t ind) const;
    /**
     * \brief Recurses to perform the pruning step of the monotone pruning
     *        algorithm
     *
     * \param marking New omega-marking processed in the current iteration of
     *        the monotone pruning algorithm.
     * \param ind Index of the omega-marking component processed.
     */
    void prune(const Marking& marking, std::size_t ind) const;
    /**
     * \brief Recurses to perform in-place acceleration of the omega-marking
     *
     * \param marking Omega-marking that must be accelerated in-place.
     * \param ind Index of the omega-marking component processed.
     */
    void accelerate(Marking& marking, std::size_t ind) const;

    /**
     * \brief Value of an element of a tuple
     */
    NbTokens val;
    /**
     * \brief Pointer to the node at the next level having the smallest value
     */
    OrderedTuplesNode* p_first_child;
    /**
     * \brief Pointer to the node at the next level having the highest value
     */
    OrderedTuplesNode* p_last_child;
    /**
     * \brief Pointer to the node at the same level having a smaller value
     */
    OrderedTuplesNode* p_prev_sibling;
    /**
     * \brief Pointer to the node at the same level having a higher value
     */
    OrderedTuplesNode* p_next_sibling;
    /**
     * \brief Pointers to monotone pruning nodes storing omega-markings equal to
     *        the tuple stored in the path to the current ordered tuples node
     *
     * This member variable is only used for the nodes at the last level of the
     * data structure and is empty for the nodes at another level. It enables
     * to keep track of the nodes in the monotone pruning reachability tree that
     * have an omega-marking equal to the tuple stored in the path to the
     * current node.
     */
    std::vector<Node*> p_nodes;
  };

  /**
   * \brief Root node of the data structure
   */
  OrderedTuplesNode root_;

  /**
   * \brief Deletes the whole data structure
   */
  inline void delete_nodes() {
    if (root_.p_last_child) {
      delete_nodes(root_.p_last_child);
      root_.p_first_child = nullptr;
      root_.p_last_child = nullptr;
    }
  }
  /**
   * \brief Deletes a node and all its descendants
   *
   * \param p_node Pointer to the node in the data structure that must be
   *        deleted with its descendants.
   */
  void delete_nodes(OrderedTuplesNode* p_node);
};

#endif /* ORDEREDTUPLES_HPP_ */
