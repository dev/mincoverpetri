/**
 * \file
 * \brief Header for tools to represent and manipulate omega-markings
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the elements used to represent and manipulate
 * omega-markings of Petri nets.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARKING_HPP_
#define MARKING_HPP_

#include <cstdint>
#include <limits>
#include <ostream>
#include <vector>


/**
 * \brief Type used to represent the number of tokens in a place of a Petri net
 *
 * \note As the class Marking can be used in some places to store the difference
 *       of two omega-markings, this type must be signed.
 */
typedef std::int8_t NbTokens;

/**
 * \brief Omega-marking of a Petri net
 *
 * This class represents an omega-marking of a Petri net, which is a record of
 * the number of tokens present in each place of the net.
 */
class Marking {
public:
  /**
   * \brief Value representing omega in omega-markings
   *
   * This is the value extending the representation of number of tokens in a
   * place of a Petri Net by verifying for any `nb_token_t` value `n`:
   * * `n < Marking::omega` if `n != Marking::omega`
   * * `Marking::omega + n = Marking::omega - n = Marking::omega`
   * * `Marking::omega <= Marking::omega`
   */
  static const NbTokens omega =
      std::numeric_limits<NbTokens>::max();

  /**
   * \brief Constructor
   *
   * \param count Number of elements in the omega-marking ie. number of places
   *        in the Petri net.
   * \param value Value used to initialize the elements of the omega-marking.
   */
  explicit Marking(std::size_t count = 0, NbTokens value = NbTokens());
  /**
   * \brief List initialization constructor
   *
   * \param init Initializer list used to initialize the elements of the
   *        omega-marking.
   */
  explicit Marking(std::initializer_list<NbTokens> init);
  /**
   * \brief Copy constructor
   *
   * \param marking Copied omega-marking.
   */
  Marking(const Marking& marking);
  /**
   * \brief Move constructor
   *
   * \param marking Moved omega-marking.
   */
  Marking(Marking&& marking);
  /**
   * \brief Destructor
   */
  ~Marking();
  /**
   * \brief Move assignment operator
   */
  Marking& operator=(Marking&&) = default;

  /**
   * \brief Equal operator
   *
   * \param marking_rhs Compared omega-marking.
   *
   * \return true if all the values of both omega-markings are equal, false
   *         otherwise.
   */
  bool operator==(const Marking& marking_rhs) const;
  /**
   * \brief Not equal operator
   *
   * \param marking_rhs Compared omega-marking.
   *
   * \return true if at least one value is different in both omega-markings,
   *         false otherwise.
   */
  bool operator!=(const Marking& marking_rhs) const;
  /**
   * \brief Less or equal operator for the product order
   *
   * The product order also known as the coordinatewise order is the partial
   * ordering induced by the cartesian product of the omega-markings.
   *
   * \param marking_rhs Compared omega-marking.
   *
   * \return true if the current omega-marking is less or equal than
   *         \p marking_rhs in the product order sense, false otherwise.
   */
  bool operator<=(const Marking& marking_rhs) const;
  /**
   * \brief Less operator for the product order
   *
   * The product order also known as the coordinatewise order is the partial
   * ordering induced by the cartesian product of the omega-markings.
   *
   * \param marking_rhs Compared omega-marking.
   *
   * \return true if the current omega-marking is less or equal than
   *         \p marking_rhs in the product order sense and both omega-markings
   *         are not equal, false otherwise.
   */
  bool operator<(const Marking& marking_rhs) const;
  /**
   * \brief Subscript operator
   *
   * \param pos Position of the element to return.
   *
   * \return Reference to the element at index \p pos.
   */
  inline NbTokens& operator[](std::size_t pos) {
    return values_[pos];
  }
  /**
   * \brief Const subscript operator
   *
   * \param pos Position of the element to return.
   *
   * \return Const reference to the element at index \p pos.
   */
  inline const NbTokens& operator[](std::size_t pos) const {
    return values_[pos];
  }

  /**
   * \brief Returns the number of elements
   *
   * \note The number of elements in an omega-marking is the number of places
   *       in the associated Petri net.
   *
   * \return Number of elements in the omega-marking.
   */
  inline const std::size_t size() const {
    return values_.size();
  }
  /**
   * \brief Adds an omega-less omega-marking
   *
   * This function makes the in-place coordinatewise addition of the values in
   * \p omegaless_marking to the values in the current omega-marking.
   *
   * \param omegaless_marking Added omega-marking that must contain no value
   *        equal to Marking::omega.
   *
   * \warning This function expects that \p omegaless_marking doesn't contain
   *          any value equal to Marking::omega and this condition is not
   *          tested by the function.
   */
  void add_omegaless_marking(const Marking& omegaless_marking);
  /**
   * \brief Switches the values in the omega-marking that verify the condition
   *        for acceleration to Marking::omega
   *
   * \param p_marking Pointer to the omega-marking compared for acceleration.
   */
  void accelerate_helper(const Marking& p_marking);

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param marking Inserted omega-marking.
   *
   * \return \p os.
   */
  friend std::ostream& operator<<(std::ostream& os, const Marking& marking);

private:
  /**
   * \brief Omega-marking values, ie. the number of tokens in each place of the
   *        Petri net
   *
   * The size of Marking::values_ is the number of places in the Petri net.
   */
  std::vector<NbTokens> values_;

  /**
   * \brief Print the value of one element of the omega-marking
   *
   * \param index Index of the printed element.
   * \param os Output stream in which the element must be printed.
   */
  void print_value(std::size_t index, std::ostream& os) const;
};

#endif /* MARKING_HPP_ */
