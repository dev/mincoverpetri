/**
 * \file
 * \brief Tools to represent and manipulate unordered collections of
 *        omega-markings
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the elements used to represent and manipulate an unoredered
 * collection of omega-markings of a Petri net. Such a collection can in
 * particular be used to represent the minimal coverability set of a Petri net.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "markingscollection.hpp"

#include <algorithm>
#include <sstream>
#include <fstream>
#include <utility>

#include "../third_party/tinyxml2.h"


MarkingsCollection::MarkingsCollection() :
    markings_(std::vector<Marking>(0)) {}

MarkingsCollection::MarkingsCollection(std::vector<Marking>&& markings) :
    markings_(std::move(markings)) {}

bool MarkingsCollection::operator==(const MarkingsCollection& markings_col_rhs)
    const {
  // This operator checks if the two marking collections contains the exact same
  // markings regardless of the order in which they are stored internally
  if (markings_.size() != markings_col_rhs.markings_.size()) {
    return false;
  }
  std::vector<bool> is_matched(markings_col_rhs.markings_.size(), false);
  for (const auto& marking_lhs : markings_) {
    std::size_t ind_marking_rhs = 0;
    bool match_found = false;
    for (const auto& marking_rhs : markings_col_rhs.markings_) {
      if (!is_matched[ind_marking_rhs]) {
        if (marking_lhs.size() != marking_rhs.size()) {
          return false;
        }
        if (marking_lhs == marking_rhs) {
          is_matched[ind_marking_rhs] = true;
          match_found = true;
          break;
        }
      }
      ++ind_marking_rhs;
    }
    if (!match_found) {
      return false;
    }
  }
  return true;
}

bool MarkingsCollection::operator!=(const MarkingsCollection& markings_col_rhs)
    const {
  return !(*this == markings_col_rhs);
}

void MarkingsCollection::read(const std::string& filename) {
  markings_.resize(0);
  tinyxml2::XMLDocument xml_doc;
  xml_doc.LoadFile(filename.c_str());
  auto mcs_elem = xml_doc.FirstChildElement("minCovSet");
  for (auto marking_elem = mcs_elem->FirstChildElement("omegaMarking");
      marking_elem;
      marking_elem = marking_elem->NextSiblingElement("omegaMarking")) {
    auto marking_text = marking_elem->GetText();
    if (marking_text == nullptr) {
      markings_.push_back(Marking(0));
      break;
    }
    std::istringstream marking_stream(marking_text);
    std::string val;
    std::size_t marking_size = 0;
    while(marking_stream >> val) {
      ++marking_size;
    }
    markings_.push_back(Marking(marking_size));
    auto& tmp_marking = markings_[markings_.size()-1];
    marking_stream = std::istringstream(marking_elem->GetText());
    std::size_t ind_marking = 0;
    while(marking_stream >> val) {
      if (val[0] == 'w') {
        tmp_marking[ind_marking] = Marking::omega;
      } else {
        // TODO: Ideally a warning should be displayed when there is an
        // overflow in the following cast
        tmp_marking[ind_marking] = static_cast<NbTokens>(std::stoll(val));
      }
      ++ind_marking;
    }
  }
}

void MarkingsCollection::write(const std::string& filename) const {
  std::ofstream out_file;
  out_file.open(filename);
  write(out_file);
  out_file.close();
}

void MarkingsCollection::write(std::ostream& out_stream) const {
  // TODO: This writer is very minimal, in particular it would be better to
  // have variables and even parameters to define the minCovSet id and the
  // omegaMarking id

  out_stream << "<?xml version=\"1.0\" ?>" << std::endl
      << "<minCovSet id=\"mcs0\">" << std::endl;
  std::size_t marking_ind = 0;
  for (const auto& marking : markings_) {
    out_stream << "  <omegaMarking id=\"om" << marking_ind++ << "\">"
        << marking << "</omegaMarking>"<< std::endl;
  }
  out_stream <<"</minCovSet>" << std::endl;
}

bool MarkingsCollection::is_antichain() const {
  for (const auto& marking_1 : markings_) {
    for (const auto& marking_2 : markings_) {
      if ((&marking_1 != &marking_2) && (marking_1 <= marking_2)) {
        return false;
      }
    }
  }
  return true;
}

bool MarkingsCollection::is_covering_set(const PetriNet& net) const {
  auto mapping_p_t = net.mapping_p_t();
  auto mapping_diff = net.mapping_diff();
  for (const auto& marking : markings_) {
    for (std::size_t ind_trans = 0, nb_trans = net.transitions().size();
        ind_trans < nb_trans; ++ind_trans) {
      if (mapping_p_t[ind_trans] <= marking) {
        Marking tmp_marking(marking);
        tmp_marking.add_omegaless_marking(mapping_diff[ind_trans]);
        if (!is_dominated(tmp_marking)) {
          return false;
        }
      }
    }
  }
  return true;
}

std::ostream& operator<<(std::ostream& os,
    const MarkingsCollection& markings_col) {
  for (const auto& marking : markings_col.markings_) {
    os << marking << std::endl;
  }
  return os;
}
