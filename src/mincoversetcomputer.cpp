/**
 * \file
 * \brief Tools to compute the minimal coverability set of a Petri net
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the elements used to compute the minimal coverability set
 * of a Petri net. In particular it contains an implementation of the monotone
 * pruning algorithm.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "mincoversetcomputer.hpp"

#include <chrono>
#include <functional>
#include <utility>


MinCoverSetComputer::MinCoverSetComputer(const PetriNet& net) :
    initial_marking_(net.initial_marking()), mapping_p_t_(net.mapping_p_t()),
    mapping_diff_(net.mapping_diff()), p_curr_node_(nullptr), p_root_(nullptr) {
}

MarkingsCollection MinCoverSetComputer::compute(
    const MonotonePruningParam& param) {
  monotone_pruning(param);
  std::vector<Marking> markings;
  for (const auto p_node : p_active_nodes()) {
    markings.push_back(p_node->marking());
  }
  MarkingsCollection min_cover_set(std::move(markings));
  delete_nodes();
  return min_cover_set;
}

void MinCoverSetComputer::monotone_pruning(const MonotonePruningParam& param) {
  // TODO: This long function should be split in shorter subfunctions.

  bool use_orderedtuples = param.use_orderedtuples_for_accelerate ||
      param.use_orderedtuples_for_is_dominated ||
      param.use_orderedtuples_for_prune;

  auto start_time = std::chrono::steady_clock::now();
  wait_.clear();
  computation_stats_.nb_wait = 0;
  computation_stats_.nb_nodes = 0;
  computation_stats_.nb_accelerated = 0;
  ord_tuples_.clear();

  p_root_ = new Node(Marking(initial_marking_));
  ++computation_stats_.nb_nodes;

  if (use_orderedtuples) {
    ord_tuples_.insert(p_root_);
  }

  p_root_->activate();

  p_curr_node_ = p_root_;
  update_wait();

  while (wait_.size()) {
    const Candidate& candidate =
        param.bfs_traversal ? wait_.front() : wait_.back();

    ++computation_stats_.nb_wait;

    p_curr_node_ = candidate.p_node;
    if (p_curr_node_->is_active()) {
      Marking tmp_marking(p_curr_node_->marking());
      tmp_marking.add_omegaless_marking(
          mapping_diff_[candidate.ind_transition]);

      param.bfs_traversal ? wait_.pop_front() : wait_.pop_back();

      if (!(param.use_orderedtuples_for_is_dominated ?
          ord_tuples_.is_dominated(tmp_marking) :
          is_dominated(tmp_marking, p_root_))) {
        // NOTE: To improve the processing speed of this implementation,
        // the following acceleration is only done after checking the condition
        // above.
        // Note that this differs from what is described in the reference paper
        // where the acceleration is systematically done before checking the
        // condition.

        // NOTE: The following lines allow to choose between
        // MinCoverSetComputer::accelerate() and OrderedTuples::accelerate()
        // to perform the acceleration step.
        // Note that both functions are not exactly equivalent (see the
        // documentation of OrderedTuples::accelerate() for more details).
        // Note also that neither of the two functions exactly implements the
        // acceleration described in the reference paper, as the acceleration
        // is done in-place here (see the documentation of
        // MinCoverSetComputer::monotone_pruning() for more details).
        if (param.use_orderedtuples_for_accelerate) {
          p_curr_node_->mark_ancestors();
          ord_tuples_.accelerate(tmp_marking);
        } else {
          accelerate(p_curr_node_, tmp_marking);
        }
        ++computation_stats_.nb_accelerated;

        // NOTE: In this implementation, a new node is created only when
        // reaching the following line. Note that this differs from what is
        // described in the reference paper (see the documentation of
        // MinCoverSetComputer::monotone_pruning() for more details).
        Node* p_new_node = new Node(std::move(tmp_marking), p_curr_node_);
        ++computation_stats_.nb_nodes;

        p_curr_node_->append_p_child(p_new_node);
        p_curr_node_ = p_new_node;

        if (param.use_orderedtuples_for_accelerate) {
          p_curr_node_->mark_as_ancestor();
        } else {
          p_curr_node_->mark_ancestors();
        }

        if (param.use_orderedtuples_for_prune) {
          ord_tuples_.prune(p_curr_node_->marking());
        } else {
          p_root_->prune(p_curr_node_->marking());
        }

        if (use_orderedtuples) {
          ord_tuples_.insert(p_curr_node_);
        }
        p_curr_node_->unmark_ancestors();
        p_curr_node_->activate();
        update_wait();
      } else {
        if (param.use_orderedtuples_for_accelerate) {
          p_curr_node_->unmark_ancestors();
        }
      }
    } else {
      param.bfs_traversal ? wait_.pop_front() : wait_.pop_back();
    }
  }

  std::chrono::duration<double> elapsed_seconds =
      std::chrono::steady_clock::now() - start_time;
  computation_stats_.computation_time = elapsed_seconds.count();

  computation_stats_.nb_min_cover_set = count_active_nodes();
}

bool MinCoverSetComputer::is_dominated(const Marking& marking,
    const Node* const p_node) {
  if (p_node->is_active()) {
    if (marking <= p_node->marking()) {
      return true;
    }
  }
  for (auto p_child : p_node->p_children()) {
    if (is_dominated(marking, p_child)) {
      return true;
    }
  }
  return false;
}

void MinCoverSetComputer::accelerate(const Node* const p_curr_node,
    Marking& marking) {
  auto p_node = p_curr_node;
  while (p_node != nullptr) {
    if (p_node->is_active() && (p_node->marking() < marking)) {
      marking.accelerate_helper(p_node->marking());
    }
    p_node = p_node->p_parent();
  }
}

void MinCoverSetComputer::update_wait() {
  for (std::size_t ind_trans = 0, nb_trans = mapping_p_t_.size();
      ind_trans < nb_trans; ++ind_trans) {
    if (mapping_p_t_[ind_trans] <= p_curr_node_->marking()) {
      wait_.push_back(Candidate{p_curr_node_, ind_trans});
    }
  }
}

int MinCoverSetComputer::count_active_nodes() const {
  int nb_active_nodes = 0;
  std::function<void(Node*)> recurs_active =
      [&nb_active_nodes, &recurs_active](Node* p_curr_node) {
        if (p_curr_node->is_active()) {
          ++nb_active_nodes;
        }
        for (auto p_node : p_curr_node->p_children()) {
          recurs_active(p_node);
        }
      };
  recurs_active(p_root_);
  return nb_active_nodes;
}

std::vector<Node*> MinCoverSetComputer::p_active_nodes() const {
  std::vector<Node*> p_nodes(0);
  std::function<void(Node*)> recurs_active =
      [&p_nodes, &recurs_active](Node* p_curr_node) {
        if (p_curr_node->is_active()) {
          p_nodes.push_back(p_curr_node);
        }
        for (auto p_node : p_curr_node->p_children()) {
          recurs_active(p_node);
        }
      };
  recurs_active(p_root_);
  return p_nodes;
}
