/**
 * \file
 * \brief Main functions of the command line application
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the main functions of the command line application
 * that provide functionality to parse the command line options and process the
 * data.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.hpp"

#include <iostream>

#include "../third_party/args.hxx"

#include "petrinet.hpp"
#include "pnmlreader.hpp"


#ifndef COMPILE_TESTS
int main(int argc, char *argv[]) {
  return parse_args_and_compute(argc, argv);
}
#endif  // not COMPILE_TESTS

int parse_args_and_compute(int argc, char *argv[]) {
  args::ArgumentParser parser(
      "Compute the minimal coverability set of a Petri net",
      "This application performs the fast computation of the minimal "
      "coverability set of Petri nets using the monotone pruning algorithm "
      "proposed in the article:\n"
      "| Minimal Coverability Set for Petri Nets: Karp and Miller Algorithm "
      "with\n| Pruning.\n"
      "| P.-A. Reynier and F. Servais.\n"
      "| In Fundamenta Informaticae, vol. 122, no. 1-2, pp. 1-30, IOS "
      "Press, 2013.\n"
      "| DOI: 10.3233/FI-2013-781.\n"
      "The input files must be place/transition (P/T) Petri nets stored in "
      "the Petri Net Markup Language (PNML) file format defined by the "
      "standard ISO/IEC 15909 Part 2. See http://www.pnml.org for details "
      "about the format.\n"
      "By default, the computed minimal coverability set is outputted to "
      "stdout in a text format where each line represents one of the "
      "omega-markings of the set. For each omega-marking, the line gives the "
      "values of the number of tokens in every places of the Petri net "
      "seperated by spaces, using the same order for places as the one used "
      "in the input PNML file. The letter w represents the value omega.\n"
      "Alternatively, using the -o option, the result can been stored in a "
      "chosen XML file.\n"
      "As an improvement over the original monotone pruning algorithm "
      "proposed in the paper cited above, this implementation can use a data"
      "structure called OrderedTuples that is taylored to speed-up the "
      "computation of some key steps of the algorithm. Its use can be turned "
      "on or off for the different steps that it can handle using the "
      "corresponding options.\n"
      "The number of tokens is internaly represented as 8 bits signed "
      "integers and the value omega is represented as the maximal available "
      "value (ie. 127). When reading the input PNML file, any initial marking "
      "in a place that is greater than or equal to 127 is considered to be "
      "omega.\n"
      "If this is too limitating for your use, you can recompile the "
      "application with an appropriate type for the number of tokens (see the "
      "file marking.hpp) and an adapted limit value representing omega in the "
      "PNML file reader (see the file pnmlreader.hpp).\n");
  parser.Prog("Usage: mincoverpetri");
  parser.helpParams.showTerminator = false;
  parser.helpParams.progindent = 0;
  parser.helpParams.descriptionindent = 0;
  parser.helpParams.flagindent = 2;
  parser.helpParams.helpindent = 17;

  args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
  args::Flag version(parser, "version",
      "Display the version number of the executable", {'v', "version"});
  args::Flag comp_time(parser, "time", "Display the computation time",
      {'t', "time"});
  args::Flag algo_stats(parser, "stats", "Display the algorithm stats",
      {'s', "stats"});
  args::ValueFlag<std::string> out_filename(parser, "filename",
      "Store the computed minimal coverability set in an XML file instead of "
      "outputting it to stdout",
      {'o', "output"});
  args::Flag check(parser, "check", "Perform sanity check on the result",
      {'c', "check"});
  args::Group traversal_type_group(parser, "Traversal:",
      args::Group::Validators::AtMostOne);
  args::Flag bfs(traversal_type_group, "bfs",
      "Breadth-first traversal (default)", {'b', "bfs"});
  args::Flag dfs(traversal_type_group, "dfs",
      "Depth-first traversal", {'d', "dfs"});
  args::Group acc_group(parser, "accelerate():",
      args::Group::Validators::AtMostOne);
  args::Flag acc(acc_group, "acc",
      "Use OrderedTuples to implement accelerate()", {"acc"});
  args::Flag no_acc(acc_group, "no_acc",
      "Don't use OrderedTuples to implement accelerate() (default)",
      {"no_acc"});
  args::Group isdom_group(parser, "is_dominated():",
      args::Group::Validators::AtMostOne);
  args::Flag isdom(isdom_group, "isdom",
      "Use OrderedTuples to implement is_dominated() (default)", {"isdom"});
  args::Flag no_isdom(isdom_group, "no_isdom",
      "Don't use OrderedTuples to implement is_dominated()", {"no_isdom"});
  args::Group prune_group(parser, "prune():",
      args::Group::Validators::AtMostOne);
  args::Flag prune(prune_group, "prune",
      "Use OrderedTuples to implement prune() (default)", {"prune"});
  args::Flag no_prune(prune_group, "no_prune",
      "Don't use OrderedTuples to implement prune()", {"no_prune"});
  args::Positional<std::string> filename(parser,
      "filename", "filename of the input P/T Petri net (PNML file)");

  try {
    parser.ParseCLI(argc, argv);
  } catch (const args::Help& error) {
    std::cout << parser;
    return 0;
  } catch (const args::ParseError& error) {
    std::cerr << error.what() << std::endl;
    std::cerr << parser;
    return 1;
  } catch (const args::ValidationError& error) {
    std::cerr << error.what() << std::endl;
    std::cerr << parser;
    return 1;
  }
  if (version) {
    std::cout << VERSION_NUMBER << std::endl;
    return 0;
  }

  MonotonePruningParam monotone_pruning_param{true, false, true, true};

  if (bfs) {
    monotone_pruning_param.bfs_traversal = true;
  }
  if (dfs) {
    monotone_pruning_param.bfs_traversal = false;
  }

  if (acc) {
    monotone_pruning_param.use_orderedtuples_for_accelerate = true;
  }
  if (no_acc) {
    monotone_pruning_param.use_orderedtuples_for_accelerate = false;
  }

  if (isdom) {
    monotone_pruning_param.use_orderedtuples_for_is_dominated = true;
  }
  if (no_isdom) {
    monotone_pruning_param.use_orderedtuples_for_is_dominated = false;
  }

  if (prune) {
    monotone_pruning_param.use_orderedtuples_for_prune = true;
  }
  if (no_prune) {
    monotone_pruning_param.use_orderedtuples_for_prune = false;
  }

  DisplayParam display_param{true, false, false, false};
  if (comp_time) {
    display_param.display_computation_time = true;
  }
  if (algo_stats) {
    display_param.display_algo_stats = true;
  }
  if (check) {
    display_param.display_sanity_check = true;
  }

  std::string output_filename("");
  if (out_filename) {
    display_param.display_min_cover_set = false;
    output_filename = args::get(out_filename);
  }

  if (filename) {
    return process_pnml_file(args::get(filename), output_filename,
        display_param, monotone_pruning_param);
  } else {
    std::cout << parser;
  }

  return 0;
}

int process_pnml_file(const std::string& input_filename,
    const std::string& output_filename,
    const DisplayParam& display_param,
    const MonotonePruningParam& monotone_pruning_param) {
  PnmlReader reader;
  auto net = reader.read(input_filename);
  MinCoverSetComputer min_cover_set_computer(net);
  auto min_cover_set = min_cover_set_computer.compute(monotone_pruning_param);

  auto comput_stats = min_cover_set_computer.computation_stats();

  if (display_param.display_min_cover_set) {
    std::cout << min_cover_set;
  }
  if (display_param.display_computation_time) {
    std::cout << "Computation time: " << comput_stats.computation_time
        << " s" << std::endl;
  }
  if (display_param.display_algo_stats) {
    std::cout << "Nb wait: " << comput_stats.nb_wait << std::endl;
    std::cout << "Nb accelerated: " << comput_stats.nb_accelerated << std::endl;
    std::cout << "Nb nodes: " << comput_stats.nb_nodes << std::endl;
    std::cout << "Nb min. cover. set: "
        << comput_stats.nb_min_cover_set << std::endl;
  }
  if (output_filename != "") {
    min_cover_set.write(output_filename);
    std::cout << "Result stored in " << output_filename << std::endl;
  }
  if (display_param.display_sanity_check) {
    std::cout << "Result is an antichain: "
        << (min_cover_set.is_antichain() ? "true" : "false") << std::endl;
    std::cout << "Result is a covering set: "
        << (min_cover_set.is_covering_set(net) ? "true" : "false") << std::endl;
  }
  return 0;
}
