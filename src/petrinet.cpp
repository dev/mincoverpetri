/**
 * \file
 * \brief Tools to represent and manipulate place/transition Petri nets
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the tools to represent and manipulate place/transition
 * Petri nets.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "petrinet.hpp"

#include <map>
#include <utility>

// For debugging purpose, the flag TRACE_PETRINETS can be defined to trace the
// construction, destruction and move of Petri nets, using:
// #define TRACE_PETRINETS

PetriNet::PetriNet() {
#ifdef TRACE_PETRINETS
  std::cout << "Default construct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

PetriNet::PetriNet(PetriNet&& net) :
    places_(std::move(net.places_)), transitions_(std::move(net.transitions_)),
    arcs_p_t_(std::move(net.arcs_p_t_)), arcs_t_p_(std::move(net.arcs_t_p_)) {
#ifdef TRACE_PETRINETS
  std::cout << "Move " << __FUNCTION__ << " " << &net << " to " << this
      << std::endl;
#endif
}

PetriNet::~PetriNet() {
#ifdef TRACE_PETRINETS
  std::cout << "Destruct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

void PetriNet::add_place(Place&& place) {
  places_.push_back(place);
}
void PetriNet::add_transition(Transition&& transition) {
  transitions_.push_back(transition);
}
void PetriNet::add_arc_p_t(ArcPT&& arc_p_t) {
  arcs_p_t_.push_back(arc_p_t);
}
void PetriNet::add_arc_t_p(ArcTP&& arc_t_p) {
  arcs_t_p_.push_back(arc_t_p);
}

Marking PetriNet::initial_marking() const {
  Marking initial_marking(places_.size());
  for (std::size_t ind=0; ind < places_.size(); ++ind) {
    initial_marking[ind] = places_[ind].initial_marking;
  }
  return initial_marking;
}

std::map<std::string, std::size_t> compute_places_ind(
    const std::deque<Place>& places) {
  std::map<std::string, std::size_t> places_ind;
  for (std::size_t ind = 0; ind < places.size(); ++ind) {
      places_ind[places[ind].id] = ind;
  }
  return places_ind;
}

std::map<std::string, std::size_t> compute_transitions_ind(
    const std::deque<Transition>& transitions) {
  std::map<std::string, std::size_t> transitions_ind;
  for (std::size_t ind = 0; ind < transitions.size(); ++ind) {
      transitions_ind[transitions[ind].id] = ind;
  }
  return transitions_ind;
}

std::vector<Marking> PetriNet::mapping_p_t() const {
  auto places_ind = compute_places_ind(places_);
  auto transitions_ind = compute_transitions_ind(transitions_);
  std::vector<Marking> mapping(transitions_.size(), Marking(places_.size(), 0));
  for (const auto& arc : arcs_p_t_) {
    mapping[transitions_ind[arc.target.id]][places_ind[arc.source.id]]
        = arc.value;
  }
  return mapping;
}

std::vector<Marking> PetriNet::mapping_t_p() const {
  auto places_ind = compute_places_ind(places_);
  auto transitions_ind = compute_transitions_ind(transitions_);
  std::vector<Marking> mapping(transitions_.size(), Marking(places_.size(), 0));
  for (const auto& arc : arcs_t_p_) {
    mapping[transitions_ind[arc.source.id]][places_ind[arc.target.id]]
        = arc.value;
  }
  return mapping;
}

std::vector<Marking> PetriNet::mapping_diff() const {
  std::vector<Marking> mapping(transitions_.size(), Marking(places_.size(), 0));

  auto map_t_p = mapping_t_p();
  auto map_p_t = mapping_p_t();

  for (std::size_t ind_trans = 0;
      ind_trans < transitions_.size();
      ++ind_trans) {
    for (std::size_t ind_place = 0;
        ind_place < places_.size();
        ++ind_place) {
      mapping[ind_trans][ind_place] = map_t_p[ind_trans][ind_place]
          - map_p_t[ind_trans][ind_place];
    }
  }
  return mapping;
}

std::ostream& operator<<(std::ostream& os, const PetriNet& net) {
  os << "Places:" << std::endl;
  for (const auto& place : net.places_) {
    os << place << std::endl;
  }
  os << "Transitions:" << std::endl;
  for (const auto& transition : net.transitions_) {
    os << transition << std::endl;
  }
  os << "Arcs places->transitions:" << std::endl;
  for (const auto& arc_p_t : net.arcs_p_t_) {
    os << arc_p_t << std::endl;
  }
  os << "Arcs transitions->places:" << std::endl;
  for (const auto& arc_t_p : net.arcs_t_p_) {
    os << arc_t_p << std::endl;
  }
  return os;
}
