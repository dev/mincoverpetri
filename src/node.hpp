/**
 * \file
 * \brief Header for tools to represent and manipulate the nodes of the tree
 *        used in the monotone pruning algorithm
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the elements used to represent and manipulate the
 * nodes and the tree used in the monotone pruning algorithm.
 *
 * These nodes store the reachable omega-markings of the Petri net during
 * the execution of the algorithm.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NODE_HPP_
#define NODE_HPP_

#include <vector>

#include "marking.hpp"


/**
 * \brief %Node for the reachability tree built by the monotone pruning
 *        algorithm
 *
 * This class provides the elements used to represent and manipulate the
 * nodes and the tree of reached omega-markings used in the monotone pruning
 * algorithm.
 */
class Node {
public:
  /**
   * \brief Constructor
   *
   * \param marking Omega-marking that must be moved into the node as its
   *        associated omega-marking.
   * \param p_parent Pointer to the parent node.
   */
  explicit Node(Marking&& marking, Node* p_parent = nullptr);
  /**
   * \brief Copy constructor deleted to insure that no accidental copy is done
   */
  Node(const Node& node) = delete;
  /**
   * \brief Move constructor deleted as it is not needed and used
   */
  Node(Node&& node) = delete;
  /**
   * \brief Destructor
   */
  ~Node();

  /**
   * \brief Returns the omega-marking associated with the node
   *
   * \return Omega-marking associated with the node.
   */
  inline const Marking& marking() const {
    return marking_;
  }
  /**
   * \brief Returns the pointer to the parent node
   *
   * \return Pointer to the parent node.
   */
  inline Node* p_parent() const {
    return p_parent_;
  }
  /**
   * \brief Checks if the node is active
   *
   * \return true if the node is active, false otherwise.
   */
  inline bool is_active() const {
    return is_active_;
  }
  /**
   * \brief Checks if the node has been marked as an ancestor
   *
   * \return true if the node has been marked, false otherwise.
   */
  inline bool is_ancestor() const {
    return is_ancestor_;
  }
  /**
   * \brief Returns the pointers to the children nodes
   *
   * \return Pointers to the children nodes.
   */
  inline const std::vector<Node*>& p_children() const {
    return p_children_;
  }

  /**
   * \brief Marks the node as active
   */
  inline void activate() {
    is_active_ = true;
  }
  /**
   * \brief Marks the node as not active
   */
  inline void deactivate() {
    is_active_ = false;
  }
  /**
   * \brief Marks the node and all its descendants as not active
   */
  void deactivate_subtree();

  /**
   * \brief Marks the node as an ancestor
   */
  inline void mark_as_ancestor() {
    is_ancestor_ = true;
  }
  /**
   * \brief Marks the node and its ancestors as being ancestors
   */
  inline void mark_ancestors() {
    set_ancestors_flags(true);
  }
  /**
   * \brief Marks the node and its ancestors as not being ancestors
   */
  inline void unmark_ancestors() {
    set_ancestors_flags(false);
  }

  /**
   * \brief Performs the pruning step of the monotone pruning algorithm on the
   *        node and its descendants
   *
   * This function deactivates the nodes that needs to be deactivated in the
   * reachability tree considering the value of the new omega-marking processed.
   *
   * \param marking New omega-marking processed in the current iteration of the
   *        monotone pruning algorithm.
   *
   * \note This function expects that the nodes of the reachability tree have
   *       been previously marked appropriately as being active and as being
   *       ancestors to perform the expected processing.
   */
  void prune(const Marking& marking);
  /**
   * \brief Deactivates the node and its descendants when the pruning conditions
   *        are met
   *
   * This function deactivates the node and its descendants in the reachability
   * tree if the needed conditions are met. It is an helper function used when
   * performing the pruning step of the monotone pruning algorithm using
   * MinCoverSetComputer::ord_tuples_.
   *
   * \note This function expects that the nodes of the reachability tree have
   *       been previously marked appropriately as being active and as being
   *       ancestors to perform the expected processing.
   */
  void ot_prune_helper();

  /**
   * \brief Appends a pointer to a new child of the node
   *
   * \param p_child Pointer to the new child node.
   */
  void append_p_child(Node* p_child);

  /**
   * \brief Deletes a node and all its descendants
   *
   * \param p_node Pointer to the node that must be deleted with its
   *        descendants.
   */
  friend void delete_subtree(Node* p_node);

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param node Inserted node.
   *
   * \return \p os.
   */
  friend std::ostream& operator<<(std::ostream& os, const Node& node);

private:
  /**
   * \brief Omega-marking associated with the node
   */
  Marking marking_;
  /**
   * \brief Pointer to the parent node
   */
  Node* p_parent_;
  /**
   * \brief Flag specifying if the node is active
   *
   * During the execution of the monotone pruning algorithm, the node is
   * activated after its creation by setting the value of this flag to true.
   *
   * It can later be deactived in the following iterations of the algorithm by
   * switching this flag to false.
   *
   * At the end of the execution of the algorithm, the expected result is given
   * by the set of nodes in the tree that are still marked as active by this
   * flag.
   */
  bool is_active_;
  /**
   * \brief Flag specifying if the node has been marked as an ancestor
   *
   * This flag is used temporarily in the steps of the algorithm that require
   * to identify the ancestors of a given node.
   *
   * It is switched back to false when the step requiring this identification
   * is completed.
   */
  bool is_ancestor_;
  /**
   * \brief Pointers to the children nodes
   */
  std::vector<Node*> p_children_;

  /**
   * \brief Set the flag Node::is_ancestor_ of the node and its ancestors to a
   *        given value
   *
   * \param val Value at which the Node::is_ancestor_ flag must be set.
   */
  inline void set_ancestors_flags(bool val) {
    Node* p_node = this;
    while (p_node != nullptr) {
      p_node->is_ancestor_ = val;
      p_node = p_node->p_parent_;
    }
  }
};

#endif /* NODE_HPP_ */
