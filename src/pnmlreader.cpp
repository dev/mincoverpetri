/**
 * \file
 * \brief Tools to read PNML Petri net files
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the tools to read place/transition (P/T) Petri net files in
 * the Petri Net Markup Language (PNML) file format defined by the standard
 * ISO/IEC 15909 Part 2.
 *
 * See http://www.pnml.org/ for details about the PNML file format.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "pnmlreader.hpp"

#include <iostream>


// TODO: Errors when reading an invalid file could be handled in a better way,
// maybe using exceptions.

PetriNet PnmlReader::read(const std::string& filename,
    long long int omega_value) {
  PetriNet net;
  tinyxml2::XMLDocument xml_doc;
  auto xml_error = xml_doc.LoadFile(filename.c_str());
  std::string error_message = "Error: The file " +  filename +
      " is not a valid PNML file";
  if (xml_error != tinyxml2::XML_SUCCESS) {
    std::cerr << error_message << std::endl;
    return net;
  }
  auto pnml_elem = xml_doc.FirstChildElement("pnml");
  if (!pnml_elem) {
    std::cerr << error_message << std::endl;
    return net;
  }
  auto net_elem = pnml_elem->FirstChildElement("net");
  if (!net_elem) {
    std::cerr << error_message << std::endl;
    return net;
  }
  page_elem_ = net_elem->FirstChildElement("page");
  if (!page_elem_) {
    std::cerr << error_message << std::endl;
    return net;
  }

  read_places(omega_value, net);
  read_transitions(net);
  read_arcs(net);

  return net;
}

void PnmlReader::read_places(long long int omega_value, PetriNet& net) const {
  // TODO: The validity of all the place elements should be tested and an
  // error should be displayed when not valid (for example when missing the id
  // attribute).
  for (auto place_elem = page_elem_->FirstChildElement("place");
      place_elem;
      place_elem = place_elem->NextSiblingElement("place")) {
    auto id = std::string(place_elem->Attribute("id"));
    long long int initial_marking_val = 0;
    auto initial_marking_elem = place_elem->FirstChildElement("initialMarking");
    if (initial_marking_elem) {
      auto text_elem = initial_marking_elem->FirstChildElement("text");
      auto initial_marking_string = std::string(text_elem->GetText());
      initial_marking_val = std::stoll(initial_marking_string);
    }
    if (initial_marking_val >= omega_value) {
      initial_marking_val = Marking::omega;
    }
    net.add_place(Place{id, static_cast<NbTokens>(initial_marking_val)});
  }
}

void PnmlReader::read_transitions(PetriNet& net) const {
  // TODO: The validity of all the transition elements should be tested and an
  // error should be displayed when not valid (for example when missing the id
  // attribute).
  for (auto transition_elem = page_elem_->FirstChildElement("transition");
      transition_elem;
      transition_elem = transition_elem->NextSiblingElement("transition")) {
    auto id = std::string(transition_elem->Attribute("id"));
    net.add_transition(Transition{id});
  }
}

void PnmlReader::read_arcs(PetriNet& net) const {
  // TODO: The validity of all the arc elements should be tested and an
  // error should be displayed when not valid (for example when missing the id
  // attribute, or when the source or target place or transition is not defined
  // in the document).
  for (auto arc_elem = page_elem_->FirstChildElement("arc");
      arc_elem;
      arc_elem = arc_elem->NextSiblingElement("arc")) {
    auto id = std::string(arc_elem->Attribute("id"));
    auto source_id = std::string(arc_elem->Attribute("source"));
    auto target_id = std::string(arc_elem->Attribute("target"));
    long long int value = 1;
    auto inscription_elem = arc_elem->FirstChildElement("inscription");
    if (inscription_elem) {
      auto text_elem = inscription_elem->FirstChildElement("text");
      auto value_string = std::string(text_elem->GetText());
      value = std::stoll(value_string);
    }

    for (const auto& place : net.places()) {
      if (place.id == source_id) {
        for (const auto& transition : net.transitions()) {
          if (transition.id == target_id) {
            // TODO: Ideally a warning should be displayed when there is an
            // overflow in the following cast
            net.add_arc_p_t(
                ArcPT{id, place, transition, static_cast<NbTokens>(value)});
            break;
          }
        }
        break;
      } else if (place.id == target_id) {
        for (const auto& transition : net.transitions()) {
          if (transition.id == source_id) {
            // TODO: Ideally a warning should be displayed when there is an
            // overflow in the following cast
            net.add_arc_t_p(
                ArcTP{id, transition, place, static_cast<NbTokens>(value)});
            break;
          }
        }
        break;
      }
    }
  }
}
