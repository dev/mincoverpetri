/**
 * \file
 * \brief Header for tools to compute the minimal coverability set of a Petri
 *        net
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the elements used to compute the minimal coverability set
 * of a Petri net.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MINCOVERSETCOMPUTER_HPP_
#define MINCOVERSETCOMPUTER_HPP_

#include <deque>
#include <vector>

#include "markingscollection.hpp"
#include "node.hpp"
#include "orderedtuples.hpp"
#include "petrinet.hpp"


/**
 * \brief %Candidate value for one iteration of the monotone pruning algorithm
 *
 * The candidates are the element stored in the waiting queue
 * MinCoverSetComputer::wait_
 */
struct Candidate {
  /**
   * \brief Pointer to a node of the reachability tree
   */
  Node* p_node;
  /**
   * \brief Index of the transition reaching the next omega-marking
   *
   * This index is the index of the transition that must be fired from the
   * omega-marking value given in the node pointed by Candidate::p_node to reach
   * the next omega-Marking value that must be processed.
   */
  std::size_t ind_transition;
};

/**
 * \brief Set of flags parametrizing the execution of the monotone pruning
 *        algorithm
 */
struct MonotonePruningParam {
  /**
   * \brief Flag setting the reachability tree traversal order:
   *        breadth-first if true, depth-first if false
   */
  bool bfs_traversal;
  /**
   * \brief Flag indicating if MinCoverSetComputer::ord_tuples_ must be used to
   *        implement the function %accelerate()
   */
  bool use_orderedtuples_for_accelerate;
  /**
   * \brief Flag indicating if MinCoverSetComputer::ord_tuples_ must be used to
   *        implement the function %is_dominated()
   */
  bool use_orderedtuples_for_is_dominated;
  /**
   * \brief Flag indicating if MinCoverSetComputer::ord_tuples_ must be used to
   *        implement the function %prune()
   */
  bool use_orderedtuples_for_prune;
};

/**
 * \brief Statistics of the computation done in the monotone pruning
 *        algorithm
 */
struct ComputationStats {
  /**
   * \brief Number of candidates added to the waiting list
   *
   * This is also the number of iterations used by the algorithm to reach the
   * solution.
   */
  int nb_wait;
  /**
   * \brief Number of omega-markings accelerated during the execution
   */
  int nb_accelerated;
  /**
   * \brief Number of nodes added to the reachability tree built by the
   *        algorithm
   */
  int nb_nodes;
  /**
   * \brief Number of omega-markings in the minimal coverability set
   */
  int nb_min_cover_set;
  /**
   * \brief Duration in seconds of the execution of the algorithm
   */
  double computation_time;  // in s
};

/**
 * \brief Class computing the minimal coverability set of a Petri net using the
 *        monotone pruning algorithm
 *
 * The details of the monotone pruning algorithm can be found in the article
 * \ref article "[Reynier]".
 */
class MinCoverSetComputer {
public:
  /**
   * \brief Constructor
   *
   * \param net Petri net for which the minimal coverability set must be
   *        computed.
   */
  explicit MinCoverSetComputer(const PetriNet& net);

  /**
   * \brief Computes the minimal coverability set using the monotone pruning
   *        algorithm
   *
   * \param param Structure parametrizing the execution of the monotone pruning
   *        algorithm.
   *
   * \return Minimal coverability set of the Petri net.
   */
  MarkingsCollection compute(const MonotonePruningParam& param);

  /**
   * \brief Returns the statistics of the computation
   *
   * \return Const reference to the structure containing the statistics of the
   *         computation.
   */
  inline const ComputationStats& computation_stats() const {
    return computation_stats_;
  }

  /**
   * \brief Checks if a new omega-marking value is dominated by the values
   *        already reached in a sub-tree of the reachability tree
   *
   * \param marking Tested new omega-marking.
   * \param p_node Pointer to the root node of the sub-tree.
   *
   * \returns true if there exists at least one active node in the sub-tree of
   *          the reachability tree which is associated with an omega-marking
   *          that is greater than or equal to \p marking (in the product order
   *          sense), false otherwise.
   */
  static bool is_dominated(const Marking& marking, const Node* const p_node);
  /**
   * \brief Performs in-place acceleration of the omega-marking
   *
   * \param p_curr_node Pointer to the node that must be considered with its
   *        ancestors for acceleration.
   * \param marking Omega-marking that must be accelerated in-place.
   *
   */
  static void accelerate(const Node* const p_curr_node, Marking& marking);

private:
  /**
   * \brief Initial omega-marking of the Petri net
   *
   * The size of MinCoverSetComputer::initial_marking_ is the number of places
   * in the Petri net.
   */
  Marking initial_marking_;
  /**
   * \brief Incidence mapping of the Petri net from places to transitions
   *
   * The size of MinCoverSetComputer::mapping_p_t_ is the number of transitions
   * of the Petri net. MinCoverSetComputer::mapping_p_t_ contains elements
   * having a size which is the number of places of the Petri net.
   */
  std::vector<Marking> mapping_p_t_;
  /**
   * \brief Difference of incidence mappings of the Petri net
   *
   * This is the difference of the mapping from transitions to places minus the
   * mapping from places to transitions.
   *
   * The size of MinCoverSetComputer::mapping_diff_ is the number of transitions
   * of the Petri net. MinCoverSetComputer::mapping_diff_ contains elements
   * having a size which is the number of places of the Petri net.
   */
  std::vector<Marking> mapping_diff_;

  /**
   * \brief Queue of candidates waiting to be processed
   *
   * \note
   *       * MinCoverSetComputer::wait_ corresponds to the set Wait in the
   *         article \ref article "[Reynier]".
   *       * MinCoverSetComputer::wait_ is of type \c std::deque as it requires
   *         a container type allowing to pop from both front and back to choose
   *         between breadth-first and depth-first traversal.
   */
  std::deque<Candidate> wait_;
  /**
   * \brief Pointer to the currently processed node in the reachability tree
   */
  Node* p_curr_node_;
  /**
   * \brief Pointer to the root node of the reachability tree
   *
   * \note The root node corresponds to the node x<sub>0</sub> in the article
   *       \ref article "[Reynier]".
   */
  Node* p_root_;
  /**
   * \brief Data structure used to speed-up key steps of the monotone pruning
   *        algorithm
   *
   * \note The effective use of this member variable depends on the parameters
   *       passed to the member function compute() through the parameter
   *       \p param.
   */
  OrderedTuples ord_tuples_;
  /**
   * \brief Structure storing the statistics of the computation
   */
  ComputationStats computation_stats_;

  /**
   * \brief Performs the monotone pruning algorithm to compute the minimal
   *        coverability set
   *
   * \param param Structure parametrizing the execution of the algorithm.
   *
   * \note This is an implementation of the algorithm proposed in the article
   *       \ref article "[Reynier]". The implementation differs from the
   *       algorithm in the article in two places:
   *       * The acceleration of the new omega-marking (cf. line 7 of algorithm
   *         2 in \ref article "[Reynier]") is only performed after
   *         checking the domination condition (cf. line 9 of algorithm 2 in
   *         \ref article "[Reynier]"). Furthermore, the acceleration of the
   *         new omega-marking is done in-place in this implementation, which
   *         can lead to some over-acceleration compared to the acceleration
   *         function described in the section 3.1 of \ref article "[Reynier]".
   *         This doesn't affect the validity of the algorithm which still
   *         converges to the right solution. It can only in the best case
   *         reduce the number of iterations needed to reach the solution
   *         compared to the version in the paper.
   *       * In the paper, a new node *n* is created and added to the set of
   *         nodes for each acceleration (cf. lines 7 and 8 of algorithm 2 in
   *         \ref article "[Reynier]"), but in the case where the condition
   *         given on line 9 is not verified, this node is never activated and
   *         therefore has no influence on the following iterations of the
   *         algorithm. In this implementation, to avoid creating and processing
   *         these useless nodes that would slow down the execution, we only
   *         create new nodes when the condition on line 9 is verified.
   */
  void monotone_pruning(const MonotonePruningParam& param);
  /**
   * \brief Deletes all the nodes of the reachabilty tree built by the monotone
   *        pruning algorithm
   */
  inline void delete_nodes() {
    delete_subtree(p_root_);
    p_root_ = nullptr;
  }
  /**
   * \brief Update the queue of candidates waiting to be processed
   *
   * This function adds the candidates with transitions reachable from the
   * omega-marking of the current node.
   */
  void update_wait();
  /**
   * \brief Counts the number of active nodes in the reachabilty tree
   *
   * \returns Number of active nodes in the reachabilty tree built by
   *          the monotone pruning algorithm.
   */
  int count_active_nodes() const;
  /**
   * \brief Returns pointers to the active nodes in the reachabilty tree
   *
   * \returns Pointers to the active nodes in the reachabilty tree built by
   *          the monotone pruning algorithm.
   */
  std::vector<Node*> p_active_nodes() const;
};

#endif /* MINCOVERSETCOMPUTER_HPP_ */
