/**
 * \file
 * \brief Header for tools to represent and manipulate place/transition Petri
 *        nets
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the tools to represent and manipulate place/transition
 * Petri nets.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PETRINET_HPP_
#define PETRINET_HPP_

#include <deque>
#include <ostream>
#include <string>
#include <vector>

#include "marking.hpp"


/**
 * \brief %Place of a place/transition Petri net
 */
struct Place {
public:
  /**
   * \brief Identifier
   */
  std::string id;
  /**
   * \brief Value of the initial marking (initial number of tokens in the place)
   */
  NbTokens initial_marking;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param place Inserted place.
   *
   * \return \p os.
   */
  inline friend std::ostream& operator<<(std::ostream& os, const Place& place) {
    os << "id: " << place.id << ", init. mark.: "
        << static_cast<signed>(place.initial_marking);
    return os;
  }
};

/**
 * \brief %Transition of a place/transition Petri net
 */
struct Transition {
public:
  /**
   * \brief Identifier
   */
  std::string id;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param transition Inserted transition.
   *
   * \return \p os.
   */
  inline friend std::ostream& operator<<(std::ostream& os,
      const Transition& transition) {
    os << "id: " << transition.id;
    return os;
  }
};

/**
 * \brief Arc from a place to a transition of a place/transition Petri net
 */
struct ArcPT {
public:
  /**
   * \brief Identifier
   */
  std::string id;
  /**
   * \brief %Place being the source of the arc
   */
  const Place& source;
  /**
   * \brief %Transition being the target of the arc
   */
  const Transition& target;
  /**
   * \brief Value of the arc
   *
   * This value indicates the number of tokens removed from the source place
   * when firing the target transition.
   */
  NbTokens value;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param arc_p_t Inserted arc from a place to a transition.
   *
   * \return \p os.
   */
  inline friend std::ostream& operator<<(std::ostream& os,
      const ArcPT& arc_p_t) {
    os << "id: " << arc_p_t.id << ", source: " << arc_p_t.source.id
        << " -> target: " << arc_p_t.target.id << ", value: "
        << static_cast<signed>(arc_p_t.value);
    return os;
  }
};

/**
 * \brief Arc from a transition to a place of a place/transition Petri net
 */
struct ArcTP {
public:
  /**
    * \brief Identifier
    */
  std::string id;
  /**
   * \brief %Transition being the source of the arc
   */
  const Transition& source;
  /**
   * \brief %Place being the target of the arc
   */
  const Place& target;
  /**
   * \brief Value of the arc
   *
   * This value indicates the number of tokens added to the target place
   * when firing the source transition.
   */
  NbTokens value;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param arc_t_p Inserted arc from a transition to a place.
   *
   * \return \p os.
   */
  inline friend std::ostream& operator<<(std::ostream& os,
      const ArcTP& arc_t_p) {
    os << "id: " << arc_t_p.id << ", source: " << arc_t_p.source.id
        << " -> target: " << arc_t_p.target.id << ", value: "
        << static_cast<signed>(arc_t_p.value);
    return os;
  }
};

/**
 * \brief Place/transition Petri net
 */
class PetriNet {
public:
  /**
   * \brief Default constructor
   *
   * This constructor creates an empty place/transition Petri net.
   */
  PetriNet();
  /**
   * \brief Copy constructor deleted to insure that no accidental copy is done
   */
  PetriNet(const PetriNet& net) = delete;
  /**
   * \brief Move constructor
   *
   * \param net Moved Petri net.
   */
  PetriNet(PetriNet&& net);
  /**
   * \brief Destructor
   */
  ~PetriNet();

  /**
   * \brief Add a new place to the Petri net
   *
   * \param place New added place.
   */
  void add_place(Place&& place);
  /**
   * \brief Add a new transition to the Petri net
   *
   * \param transition New added transition.
   */
  void add_transition(Transition&& transition);
  /**
   * \brief Add a new arc from an existing place to an existing transition to
   *        the Petri net
   *
   * \note The place and the transition referenced in the arc must have been
   *       previously added to the Petri net.
   *
   * \param arc_p_t New added arc.
   */
  void add_arc_p_t(ArcPT&& arc_p_t);
  /**
   * \brief Add a new arc from an existing transition to an existing place to
   *        the Petri net
   *
   * \note The place and the transition referenced in the arc must have been
   *       previously added to the Petri net.
   *
   * \param arc_t_p New added arc.
   */
  void add_arc_t_p(ArcTP&& arc_t_p);

  /**
   * \brief Returns the places of the Petri net
   *
   * \return Places of the Petri net.
   */
  inline const std::deque<Place>& places() const {
    return places_;
  }
  /**
   * \brief Returns the transitions of the Petri net
   *
   * \return Transitions of the Petri net.
   */
  inline const std::deque<Transition>& transitions() const {
    return transitions_;
  }
  /**
   * \brief Returns the arcs from places to transitions of the Petri net
   *
   * \return Arcs from places to transitions of the Petri net.
   */
  inline const std::vector<ArcPT>& arcs_p_t() const {
    return arcs_p_t_;
  }
  /**
   * \brief Returns the arcs from transitions to places of the Petri net
   *
   * \return Arcs from transitions to places of the Petri net.
   */
  inline const std::vector<ArcTP>& arcs_t_p() const {
    return arcs_t_p_;
  }

  /**
   * \brief Computes the incidence mapping from places to transitions
   *
   * \return Incidence mapping from places to transitions, which size is the
   *         number of transitions of the Petri net, and which contains elements
   *         having a size which is the number of places of the Petri net.
   */
  std::vector<Marking> mapping_p_t() const;
  /**
   * \brief Computes the incidence mapping from transitions to places
   *
   * \return Incidence mapping from transitions to places, which size is the
   *         number of transitions of the Petri net, and which contains elements
   *         having a size which is the number of places of the Petri net.
   */
  std::vector<Marking> mapping_t_p() const;
  /**
   * \brief Computes the difference of the incidence mappings
   *
   * This is the difference of the mapping from transitions to places minus the
   * mapping from places to transitions.
   *
   * \return Difference of the incidence mappings, which size is the number of
   *         transitions of the Petri net, and which contains elements having a
   *         size which is the number of places of the Petri net.
   */
  std::vector<Marking> mapping_diff() const;

  /**
   * \brief Returns the initial omega-marking of the Petri net
   *
   * \return Initial omega-marking, which size is the number of places in the
   *         Petri net.
   */
  Marking initial_marking() const;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param net Inserted Petri net.
   *
   * \return \p os.
   */
  friend std::ostream& operator<<(std::ostream& os, const PetriNet& net);

private:
  /**
   * \brief Places of the Petri net
   *
   * \note The container deque (and not vector) is used to insure that
   *       references used in arcs are not changed when places are added.
   */
  std::deque<Place> places_;
  /**
   * \brief Transitions of the Petri net
   *
   * \note The container deque (and not vector) is used to insure that
   *       references used in arcs are not changed when transitions are added.
   */
  std::deque<Transition> transitions_;
  /**
   * \brief Arcs from places to transitions of the Petri net
   */
  std::vector<ArcPT> arcs_p_t_;
  /**
   * \brief Arcs from transitions to places of the Petri net
   */
  std::vector<ArcTP> arcs_t_p_;
};

#endif /* PETRINET_HPP_ */
