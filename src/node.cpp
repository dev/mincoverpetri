/**
 * \file
 * \brief Tools to represent and manipulate the nodes and the tree used in the
 *        monotone pruning algorithm
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines the elements used to represent and manipulate the
 * nodes and the tree used in the monotone pruning algorithm.
 *
 * These nodes store the reachable omega-markings of the Petri net during
 * the execution of the algorithm.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "node.hpp"

#include <utility>


// For debugging purpose, the flag TRACE_NODES can be defined to trace the
// construction and destruction of the nodes, using:
// #define TRACE_NODES

Node::Node(Marking&& marking, Node* p_parent) :
  marking_(std::move(marking)), p_parent_(p_parent), is_active_(false),
  is_ancestor_(false) {
#ifdef TRACE_NODES
  std::cout << "Construct " << __FUNCTION__ << " " << this
      << " with marking move " << std::endl;
#endif
}

Node::~Node() {
#ifdef TRACE_NODES
  std::cout << "Destruct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

void Node::deactivate_subtree() {
  deactivate();
  for (auto p_child : p_children_) {
    p_child->deactivate_subtree();
  }
}

void Node::prune(const Marking& marking) {
  if ((!is_ancestor_ || is_active_) && marking_ <= marking) {
    deactivate_subtree();
  } else {
    for (auto p_child : p_children_) {
      p_child->prune(marking);
    }
  }
}

void Node::ot_prune_helper() {
  if (!is_ancestor_ || is_active_) {
    deactivate_subtree();
  }
}

void Node::append_p_child(Node* p_child) {
  p_children_.push_back(p_child);
}

void delete_subtree(Node* p_node) {
  for (auto p_child : p_node->p_children_) {
    delete_subtree(p_child);
  }
  delete p_node;
}

std::ostream& operator<<(std::ostream& os, const Node& node) {
  os << &node << " " << node.marking_
      << " (" << (node.is_active_ ? "active" : "inactive")
      << ", children:";
  for (auto p_child : node.p_children_) {
    os << " " << p_child;
  }
  os << ")";
  return os;
}
