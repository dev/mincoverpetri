/**
 * \file
 * \brief Tools to represent and manipulate ordered tuples used to accelerate
 *        key steps of the monotone pruning algorithm
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file defines a data structure representing ordered tuples. It provides
 * associated functionalities specifically designed to accelerate key steps of
 * the monotone pruning algorithm.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#include "orderedtuples.hpp"


// For debugging purpose, the flag TRACE_ORDEREDTUPLES can be defined to trace
// the construction and destruction of the OrderedTuples objects, using:
// #define TRACE_ORDEREDTUPLES

OrderedTuples::OrderedTuples() : root_(OrderedTuplesNode(-1)) {
#ifdef TRACE_ORDEREDTUPLES
  std::cout << "Construct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

OrderedTuples::~OrderedTuples() {
  delete_nodes();
#ifdef TRACE_ORDEREDTUPLES
  std::cout << "Destruct " << __FUNCTION__ << " " << this << std::endl;
#endif
}

void OrderedTuples::insert(Node* p_node) {
  if (p_node == nullptr || p_node->marking().size() == 0) {
    return;
  }
  root_.insert(p_node);
}

bool OrderedTuples::is_dominated(const Marking& marking) const {
  if (root_.p_last_child) {
    return root_.p_last_child->is_dominated(marking, 0);
  }
  return false;
}

void OrderedTuples::prune(const Marking& marking) const {
  if (root_.p_first_child) {
    root_.p_first_child->prune(marking, 0);
  }
}

void OrderedTuples::accelerate(Marking& marking) const {
  if (root_.p_first_child) {
    root_.p_first_child->accelerate(marking, 0);
  }
}

void OrderedTuples::delete_nodes(OrderedTuplesNode* p_node) {
  if (p_node->p_last_child) {
    OrderedTuples::delete_nodes(p_node->p_last_child);
  }
  if (p_node->p_prev_sibling) {
    OrderedTuples::delete_nodes(p_node->p_prev_sibling);
  }
  delete p_node;
}

void OrderedTuples::OrderedTuplesNode::insert(Node* p_node) {
  auto p_new = p_last_child;
  if (!p_last_child) {
    p_new = new OrderedTuplesNode(p_node->marking()[0]);
    p_first_child = p_new;
    p_last_child = p_new;
  }
  p_new->insert(p_node, 0, this);
}

void OrderedTuples::OrderedTuplesNode::insert(Node* p_node,
    std::size_t ind, OrderedTuplesNode* p_parent) {
  // TODO: For improved readability it would be good to decompose the algorithm
  // below into subfunctions with meaningful names
  auto p_new = p_last_child;
  if (val == p_node->marking()[ind]) {
    if (ind == p_node->marking().size()-1) {
      p_nodes.push_back(p_node);
    } else {
      if (!p_last_child) {
        p_last_child = new OrderedTuplesNode(p_node->marking()[ind+1]);
        p_first_child = p_last_child;
      }
      p_last_child->insert(p_node, ind+1, this);
    }
  } else if (val < p_node->marking()[ind]) {
    p_new = new OrderedTuplesNode(p_node->marking()[ind]);
    if (!p_next_sibling) {
      p_parent->p_last_child = p_new;
    } else {  // p_next_sibling->val > node.marking()[ind]
      auto old_next_sibling = p_next_sibling;
      p_new->p_next_sibling = old_next_sibling;
      old_next_sibling->p_prev_sibling = p_new;
    }
    p_next_sibling = p_new;
    p_new->p_prev_sibling = this;
    p_new->insert(p_node, ind, p_parent);
  } else {  // val > node.marking()[ind]
    if (!p_prev_sibling) {
      p_new = new OrderedTuplesNode(p_node->marking()[ind]);
      p_prev_sibling = p_new;
      p_new->p_next_sibling = this;
      p_parent->p_first_child = p_new;
    }
    p_prev_sibling->insert(p_node, ind, p_parent);
  }
}

bool OrderedTuples::OrderedTuplesNode::is_dominated(const Marking& marking,
    std::size_t ind) const {
  // TODO: For improved readability it would be good to decompose the algorithm
  // below into subfunctions with meaningful names
  if (val < marking[ind]) {
    return false;
  }
  if (!p_last_child) {
    for (auto& p_node : p_nodes) {
      if (p_node->is_active()) {
        return true;
      }
    }
    return false;
  }
  if (p_last_child->is_dominated(marking, ind+1)) {
    return true;
  }
  if (p_prev_sibling) {
    return p_prev_sibling->is_dominated(marking, ind);
  }
  return false;
}

void OrderedTuples::OrderedTuplesNode::prune(const Marking& marking,
    std::size_t ind) const {
  // TODO: For improved readability it would be good to decompose the algorithm
  // below into subfunctions with meaningful names
  if (val > marking[ind]) {
    return;
  }
  if (!p_first_child) {
    for (auto p_node : p_nodes) {
      p_node->ot_prune_helper();
    }
  } else {
    p_first_child->prune(marking, ind+1);
  }
  if (p_next_sibling) {
    p_next_sibling->prune(marking, ind);
  }
}

void OrderedTuples::OrderedTuplesNode::accelerate(Marking& marking,
    std::size_t ind) const {

  if (val > marking[ind]) {
    return;
  }
  if (!p_first_child) {
    for (auto p_node : p_nodes) {
      if (p_node->is_ancestor() && p_node->is_active()) {
        marking.accelerate_helper(p_node->marking());
        return;
      }
    }
  } else {
    p_first_child->accelerate(marking, ind+1);
  }
  if (p_next_sibling) {
    p_next_sibling->accelerate(marking, ind);
  }
}
