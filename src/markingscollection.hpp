/**
 * \file
 * \brief Header for tools to represent and manipulate unordered collections of
 *        omega-markings
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This file declares the elements used to represent and manipulate an unordered
 * collection of omega-markings of a Petri net. Such a collection can in
 * particular be used to represent the minimal coverability set of a Petri net.
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARKINGSCOLLECTION_HPP_
#define MARKINGSCOLLECTION_HPP_

#include <ostream>
#include <string>
#include <vector>

#include "marking.hpp"
#include "petrinet.hpp"


/**
 * \brief Unordered collection of omega-markings of a Petri net
 *
 * This class represents an unordered collection of omega-markings of a Petri
 * net.
 *
 * It can in particular be used to represent the minimal coverability set of a
 * Petri net.
 *
 * \note To read and write a collection of omega-markings as an XML file, this
 *       class uses the TinyXML-2 library.
 *       More details can be found here:
 *       http://www.grinninglizard.com/tinyxml2.
 */
class MarkingsCollection {
public:
  /**
   * \brief Default constructor
   *
   * Creates a collection with zero elements.
   */
  MarkingsCollection();

  /**
   * \brief Constructor
   *
   * \param markings Omega-markings that must be moved in the collection.
   */
  explicit MarkingsCollection(std::vector<Marking>&& markings);

  /**
   * \brief Copy constructor deleted to insure that no accidental copy is done
   */
  MarkingsCollection(const MarkingsCollection& markings_col) = delete;
  /**
   * \brief Move constructor
   *
   * \param markings_col Moved collection of omega-markings.
   */
  MarkingsCollection(MarkingsCollection&& markings_col) = default;

  /**
   * \brief Equal operator
   *
   * \param markings_col_rhs Compared collection of omega-markings.
   *
   * \return true if both collections contain the same omega-markings
   *         (regardless of the order in which they are internally stored),
   *         false otherwise.
   */
  bool operator==(const MarkingsCollection& markings_col_rhs) const;
  /**
   * \brief Not equal operator
   *
   * \param markings_col_rhs Compared collection of omega-markings.
   *
   * \return true if the two collections do not contain the same omega-markings
   *         (regardless of the order in which they are internally stored),
   *         false otherwise.
   */
  bool operator!=(const MarkingsCollection& markings_col_rhs) const;

  /**
   * \brief Reads a collection of omega-markings from an XML file
   *
   * \param filename Filename of the input XML file.
   */
  void read(const std::string& filename);
  /**
   * \brief Writes a collection of omega-markings to an XML file
   *
   * \param filename Filename of the output XML file.
   */
  void write(const std::string& filename) const;
  /**
   * \brief Writes a collection of omega-markings in an XML format to an output
   *        stream
   *
   * \param out_stream Character output stream.
   */
  void write(std::ostream& out_stream) const;
  /**
   * \brief Checks if the collection of omega-markings is an antichain
   *
   * An antichain is a subset of a partially ordered set such that any two
   * elements in the subset are incomparable.
   *
   * \return true if the collection if an antichain, false otherwise.
   */
  bool is_antichain() const;
  /**
   * \brief Checks if the collection of omega-markings is covering the output of
   *        a given Petri net
   *
   * This function tests if starting from any omega-marking of the collection
   * and firing any possible transition of the given Petri net leads to an
   * omega-marking that is covered by an omega-marking of the collection.
   *
   * \param net Petri net for which the test should be performed.
   *
   * \return true if the collection is covering the outputs of \p net, false
   *         otherwise.
   */
  bool is_covering_set(const PetriNet& net) const;

  /**
   * \brief Stream insertion operator
   *
   * \param os Character output stream.
   * \param markings_col Inserted collection of omega-marking.
   *
   * \return \p os.
   */
  friend std::ostream& operator<<(std::ostream& os,
      const MarkingsCollection& markings_col);

private:
  /**
   * \brief Values of the omega-markings in the collection
   *
   * The size of MarkingsCollection::markings_ is the number of omega-markings
   * in the collection, and the size of one element of this collection is the
   * number of places in the Petri net.
   *
   * \note The order in which the omega-markings are stored in
   *       MarkingsCollection::markings_ is arbitrary and meaningless.
   */
  std::vector<Marking> markings_;

  /**
   * \brief Checks if an omega-marking is dominated by the omega-markings in the
   *        collection
   *
   * \param marking Tested omega-marking.
   *
   * \return true if there exists at least one omega-marking in the collection
   *         that is greater than or equal to the tested omega-marking, false
   *         otherwise.
   */
  inline bool is_dominated(const Marking& marking) const {
    for (const auto& tmp_marking : markings_) {
      if (marking <= tmp_marking) {
        return true;
      }
    }
    return false;
  }
};

#endif /* MARKINGSCOLLECTION_HPP_ */
