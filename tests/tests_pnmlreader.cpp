/**
 * \file tests_pnmlreader.cpp
 * \brief Tests for the code in pnmlreader.hpp and pnmlreader.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files pnmlreader.hpp and
 * pnmlreader.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include <iostream>
#include <sstream>

#include "../third_party/catch.hpp"

#include "../src/pnmlreader.hpp"


TEST_CASE("PnmlReader::read()", "[pnmlreader]") {
  SECTION("Example 00.pnml (empty Petri net)") {
    PnmlReader reader;
    std::string filename = TEST_DATA_PATH "00.pnml";
    auto net = reader.read(filename);

    REQUIRE(net.places().size() == 0);
    REQUIRE(net.transitions().size() == 0);
    REQUIRE(net.arcs_p_t().size() == 0);
    REQUIRE(net.arcs_t_p().size() == 0);

  }

  SECTION("Example 01.pnml, default omega_value") {
    PnmlReader reader;
    std::string filename = TEST_DATA_PATH "01.pnml";
    Marking expected_marking;
    auto net = reader.read(filename);

    REQUIRE(net.places().size() == 3);
    REQUIRE(net.transitions().size() == 2);
    REQUIRE(net.arcs_p_t().size() == 2);
    REQUIRE(net.arcs_t_p().size() == 2);

    REQUIRE(net.places()[0].id == "p0");
    REQUIRE(net.places()[1].id == "p1");
    REQUIRE(net.places()[2].id == "p2");

    REQUIRE(net.transitions()[0].id == "t0");
    REQUIRE(net.transitions()[1].id == "t1");

    REQUIRE(net.arcs_p_t()[0].id == "p0:t0");
    REQUIRE(net.arcs_p_t()[1].id == "p0:t1");

    REQUIRE(net.arcs_t_p()[0].id == "t1:p2");
    REQUIRE(net.arcs_t_p()[1].id == "t0:p1");

    expected_marking = Marking{0, 5, Marking::omega};
    REQUIRE(net.initial_marking() == expected_marking);
    expected_marking = Marking{1, 0, 0};
    REQUIRE(net.mapping_p_t()[0] == expected_marking);
    expected_marking = Marking{2, 0, 0};
    REQUIRE(net.mapping_p_t()[1] == expected_marking);
    expected_marking = Marking{0, 3, 0};
    REQUIRE(net.mapping_t_p()[0] == expected_marking);
    expected_marking = Marking{0, 0, 1};
    REQUIRE(net.mapping_t_p()[1] == expected_marking);
  }

  SECTION("Example 01.pnml, specified omega_value") {
    PnmlReader reader;
    std::string filename = TEST_DATA_PATH "01.pnml";
    Marking expected_marking;
    long long int omega_value = 4;
    auto net = reader.read(filename, omega_value);

    REQUIRE(net.places().size() == 3);
    REQUIRE(net.transitions().size() == 2);
    REQUIRE(net.arcs_p_t().size() == 2);
    REQUIRE(net.arcs_t_p().size() == 2);

    REQUIRE(net.places()[0].id == "p0");
    REQUIRE(net.places()[1].id == "p1");
    REQUIRE(net.places()[2].id == "p2");

    REQUIRE(net.transitions()[0].id == "t0");
    REQUIRE(net.transitions()[1].id == "t1");

    REQUIRE(net.arcs_p_t()[0].id == "p0:t0");
    REQUIRE(net.arcs_p_t()[1].id == "p0:t1");

    REQUIRE(net.arcs_t_p()[0].id == "t1:p2");
    REQUIRE(net.arcs_t_p()[1].id == "t0:p1");

    expected_marking = Marking{0, Marking::omega, Marking::omega};
    REQUIRE(net.initial_marking() == expected_marking);
    expected_marking = Marking{1, 0, 0};
    REQUIRE(net.mapping_p_t()[0] == expected_marking);
    expected_marking = Marking{2, 0, 0};
    REQUIRE(net.mapping_p_t()[1] == expected_marking);
    expected_marking = Marking{0, 3, 0};
    REQUIRE(net.mapping_t_p()[0] == expected_marking);
    expected_marking = Marking{0, 0, 1};
    REQUIRE(net.mapping_t_p()[1] == expected_marking);
  }

  SECTION("Example 02.pnml (swimming pool model from pnml.org)") {
    PnmlReader reader;
    std::string filename = TEST_DATA_PATH "02.pnml";
    Marking expected_marking;
    auto net = reader.read(filename);

    REQUIRE(net.places().size() == 9);
    REQUIRE(net.transitions().size() == 7);
    REQUIRE(net.arcs_p_t().size() == 10);
    REQUIRE(net.arcs_t_p().size() == 10);

    REQUIRE(net.places()[0].id == "cId-773840572439763225716");
    REQUIRE(net.places()[1].id == "cId-78193774406698601953");
    REQUIRE(net.places()[2].id == "cId-777476037234455225817");
    REQUIRE(net.places()[3].id == "cId-77747603723445522582");
    REQUIRE(net.places()[4].id == "cId-77747603723445522588");
    REQUIRE(net.places()[5].id == "cId-77747603723445522587");
    REQUIRE(net.places()[6].id == "cId-77747603723445522586");
    REQUIRE(net.places()[7].id == "cId-77747603723445522584");
    REQUIRE(net.places()[8].id == "cId-77764128598562036825");

    REQUIRE(net.transitions()[0].id == "cId-777641285985620368215");
    REQUIRE(net.transitions()[1].id == "cId-778302279272294019514");
    REQUIRE(net.transitions()[2].id == "cId-778302279272294019512");
    REQUIRE(net.transitions()[3].id == "cId-778302279272294019513");
    REQUIRE(net.transitions()[4].id == "cId-77830227927229401959");
    REQUIRE(net.transitions()[5].id == "cId-778302279272294019511");
    REQUIRE(net.transitions()[6].id == "cId-778302279272294019510");

    REQUIRE(net.arcs_p_t()[0].id == "cId-778302279272294019530");
    REQUIRE(net.arcs_p_t()[1].id == "cId-779459017953469638734");
    REQUIRE(net.arcs_p_t()[2].id == "cId-779624266704634781128");
    REQUIRE(net.arcs_p_t()[3].id == "cId-779624266704634781124");
    REQUIRE(net.arcs_p_t()[4].id == "cId-779624266704634781136");
    REQUIRE(net.arcs_p_t()[5].id == "cId-779624266704634781126");
    REQUIRE(net.arcs_p_t()[6].id == "cId-779624266704634781148");
    REQUIRE(net.arcs_p_t()[7].id == "cId-779624266704634781118");
    REQUIRE(net.arcs_p_t()[8].id == "cId-779624266704634781122");
    REQUIRE(net.arcs_p_t()[9].id == "cId-779128521310132813120");

    REQUIRE(net.arcs_t_p()[0].id == "cId-779459017953469638731");
    REQUIRE(net.arcs_t_p()[1].id == "cId-779459017953469638729");
    REQUIRE(net.arcs_t_p()[2].id == "cId-779459017953469638727");
    REQUIRE(net.arcs_t_p()[3].id == "cId-779624266704634781123");
    REQUIRE(net.arcs_t_p()[4].id == "cId-779624266704634781135");
    REQUIRE(net.arcs_t_p()[5].id == "cId-779624266704634781133");
    REQUIRE(net.arcs_t_p()[6].id == "cId-779624266704634781125");
    REQUIRE(net.arcs_t_p()[7].id == "cId-779624266704634781137");
    REQUIRE(net.arcs_t_p()[8].id == "cId-779624266704634781121");
    REQUIRE(net.arcs_t_p()[9].id == "cId-779624266704634781119");

    expected_marking = Marking{1, 0, 1, 3, 0, 0, 0, 0, 0};
    REQUIRE(net.initial_marking() == expected_marking);

    expected_marking = Marking{0, 0, 0, 0, 1, 0, 0, 0, 0};
    REQUIRE(net.mapping_p_t()[0] == expected_marking);
    expected_marking = Marking{0, 0, 0, 0, 0, 1, 0, 0, 0};
    REQUIRE(net.mapping_p_t()[1] == expected_marking);
    expected_marking = Marking{1, 0, 0, 0, 0, 0, 0, 0, 1};
    REQUIRE(net.mapping_p_t()[2] == expected_marking);
    expected_marking = Marking{0, 0, 0, 0, 0, 0, 1, 0, 0};
    REQUIRE(net.mapping_p_t()[3] == expected_marking);
    expected_marking = Marking{1, 0, 0, 1, 0, 0, 0, 0, 0};
    REQUIRE(net.mapping_p_t()[4] == expected_marking);
    expected_marking = Marking{0, 0, 0, 0, 0, 0, 0, 1, 0};
    REQUIRE(net.mapping_p_t()[5] == expected_marking);
    expected_marking = Marking{0, 1, 1, 0, 0, 0, 0, 0, 0};
    REQUIRE(net.mapping_p_t()[6] == expected_marking);

    expected_marking = Marking{0, 0, 0, 1, 0, 0, 0, 0, 0};
    REQUIRE(net.mapping_t_p()[0] == expected_marking);
    expected_marking = Marking{1, 0, 0, 0, 1, 0, 0, 0, 0};
    REQUIRE(net.mapping_t_p()[1] == expected_marking);
    expected_marking = Marking{0, 0, 0, 0, 0, 0, 1, 0, 0};
    REQUIRE(net.mapping_t_p()[2] == expected_marking);
    expected_marking = Marking{0, 0, 1, 0, 0, 1, 0, 0, 0};
    REQUIRE(net.mapping_t_p()[3] == expected_marking);
    expected_marking = Marking{0, 1, 0, 0, 0, 0, 0, 0, 0};
    REQUIRE(net.mapping_t_p()[4] == expected_marking);
    expected_marking = Marking{1, 0, 0, 0, 0, 0, 0, 0, 1};
    REQUIRE(net.mapping_t_p()[5] == expected_marking);
    expected_marking = Marking{0, 0, 0, 0, 0, 0, 0, 1, 0};
    REQUIRE(net.mapping_t_p()[6] == expected_marking);
  }

  SECTION("Invalid input files") {
    std::vector<std::string> filenames{TEST_DATA_PATH "invalid_01.pnml",
        TEST_DATA_PATH "invalid_02.pnml", TEST_DATA_PATH "invalid_03.pnml",
        TEST_DATA_PATH "invalid_04.pnml"};

    for (const auto& filename : filenames) {
      PnmlReader reader;

      // Temporary redirecting stderr in a buffer
      std::stringstream buffer_cerr;
      std::streambuf* old_cerr = std::cerr.rdbuf(buffer_cerr.rdbuf());

      auto net = reader.read(filename);

      // Restoring stderr
      std::cerr.rdbuf(old_cerr);

      std::string str_cerr = buffer_cerr.str();

      // Check that an error message is written to std::err
      REQUIRE(str_cerr.size() > 10);

      // Check that the returned net is empty
      REQUIRE(net.places().size() == 0);
      REQUIRE(net.transitions().size() == 0);
      REQUIRE(net.arcs_p_t().size() == 0);
      REQUIRE(net.arcs_t_p().size() == 0);
    }
  }
}

#endif  // COMPILE_TESTS
