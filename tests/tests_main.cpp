/**
 * \file tests_main.cpp
 * \brief Tests for the code in main.hpp and main.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files main.hpp and
 * main.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include <iostream>
#include <sstream>

#include "../third_party/catch.hpp"

#include "../src/main.hpp"


TEST_CASE("parse_args_and_compute()", "[main]") {
  int min_help_size = 100;

  char command_name[] = "mincoverpetri";
  char help[] = "-h";
  char version[] = "-v";
  char comp_time[] = "-t";
  char stats[] = "-s";
  char output[] = "-o";
  char output_filename[] = TEST_DATA_PATH "tmp_out.xml";
  char check[] = "-c";
  char bfs[] = "-b";
  char dfs[] = "-d";
  char acc[] = "--acc";
  char no_acc[] = "--no_acc";
  char isdom[] = "--isdom";
  char no_isdom[] = "--no_isdom";
  char prune[] = "--prune";
  char no_prune[] = "--no_prune";
  char filename[] = TEST_DATA_PATH "07.pnml";

  std::string expected_bfs =
      "1 0 0 0 0 0 0\n"
      "0 0 0 0 0 1 0\n"
      "0 0 0 1 w 0 0\n"
      "0 0 1 0 w 0 0\n"
      "0 0 0 0 0 0 1\n"
      "0 1 0 0 1 0 0\n";
  std::string expected_dfs =
      "1 0 0 0 0 0 0\n"
      "0 0 0 0 0 0 1\n"
      "0 1 0 0 1 0 0\n"
      "0 0 1 0 w 0 0\n"
      "0 0 0 1 w 0 0\n"
      "0 0 0 0 0 1 0\n";

  // Temporary redirecting stdout in a buffer
  std::stringstream buffer_cout;
  std::streambuf* old_cout = std::cout.rdbuf(buffer_cout.rdbuf());

  SECTION("Check that the version number is displayed with -v") {
    char* argv[] = {command_name, version, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout == VERSION_NUMBER "\n");
  }

  SECTION("Check that some help is displayed with -h") {
    char* argv[] = {command_name, help, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout.size() > min_help_size);
  }

  SECTION("Check that the help is displayed when no filename is given") {
    char* argv_help[] = {command_name, help, nullptr};
    int  argc_help = static_cast<int>(sizeof(argv_help) /
        sizeof(argv_help[0])) - 1;
    parse_args_and_compute(argc_help, argv_help);

    std::string str_cout_help = buffer_cout.str();

    buffer_cout.str("");

    char* argv[] = {command_name, nullptr};
    int argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout.size() > min_help_size);
    REQUIRE(str_cout == str_cout_help);
  }

  SECTION("Check the result with default parameters") {
    char* argv[] = {command_name, filename, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout == expected_bfs);
  }

  SECTION("Check that algorithm options don't change the result") {
    SECTION("Traversal BFS, all options true") {
      char* argv[] = {command_name, bfs, acc, isdom, prune, filename, nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      REQUIRE(str_cout == expected_bfs);
    }

    SECTION("Traversal BFS, all options false") {
      char* argv[] = {command_name, bfs, no_acc, no_isdom, no_prune, filename,
          nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      REQUIRE(str_cout == expected_bfs);
    }

    SECTION("Traversal DFS, all options true") {
      char* argv[] = {command_name, dfs, acc, isdom, prune, filename, nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      REQUIRE(str_cout == expected_dfs);
    }

    SECTION("Traversal DFS, all options false") {
      char* argv[] = {command_name, dfs, no_acc, no_isdom, no_prune, filename,
          nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      REQUIRE(str_cout == expected_dfs);
    }
  }

  SECTION("Check that computation time is displayed") {
    char* argv[] = {command_name, comp_time, filename, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout.find("Computation time:") != std::string::npos);
  }

  SECTION("Check that the stats are displayed with the right values") {
    SECTION("BFS traversal") {
      char* argv[] = {command_name, bfs, stats, filename, nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      std::string expected_stats_bfs =
          "Nb wait: 11\n"
          "Nb accelerated: 8\n"
          "Nb nodes: 9\n"
          "Nb min. cover. set: 6\n";

      REQUIRE(str_cout.find(expected_stats_bfs) != std::string::npos);
    }

    SECTION("DFS traversal") {
      char* argv[] = {command_name, dfs, stats, filename, nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      parse_args_and_compute(argc, argv);

      // Restoring stdout
      std::cout.rdbuf(old_cout);

      std::string str_cout = buffer_cout.str();

      std::string expected_stats_dfs =
          "Nb wait: 15\n"
          "Nb accelerated: 12\n"
          "Nb nodes: 13\n"
          "Nb min. cover. set: 6\n";

      REQUIRE(str_cout.find(expected_stats_dfs) != std::string::npos);
    }
  }

  SECTION("Check that the sanity check result is displayed") {
    char* argv[] = {command_name, check, filename, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    std::string expected =
        "Result is an antichain: true\n"
        "Result is a covering set: true\n";

    REQUIRE(str_cout.find(expected) != std::string::npos);
  }

  SECTION("Test saving output in a file") {
    char* argv[] = {command_name, output, output_filename, filename, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);

    std::string str_cout = buffer_cout.str();

    MarkingsCollection mark_collec;
    mark_collec.read(output_filename);

    std::remove(output_filename);

    REQUIRE(str_cout.find(output_filename) != std::string::npos);

    std::string filename_str(filename);
    MarkingsCollection mark_collec_ref;

    mark_collec_ref.read(
        filename_str.substr(0, filename_str.size()-5) + "_solution.xml");

    REQUIRE(mark_collec == mark_collec_ref);
  }

  SECTION("Check that unexpected options are detected") {
    // Temporary redirecting stderr in a buffer
    std::stringstream buffer_cerr;
    std::streambuf* old_cerr = std::cerr.rdbuf(buffer_cerr.rdbuf());

    std::vector<std::string> flags{"-z", "--unexpected", "text"};

    for (auto& flag : flags) {
      buffer_cout.str("");
      buffer_cerr.str("");

      char* argv[] = {command_name, &flag[0], filename, nullptr};
      int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
      auto out = parse_args_and_compute(argc, argv);

      REQUIRE(out > 0);

      std::string str_cout = buffer_cout.str();

      REQUIRE(str_cout.size() == 0);

      std::string str_cerr = buffer_cerr.str();

      REQUIRE(str_cerr.size() > min_help_size);
    }

    // Restoring stdout
    std::cout.rdbuf(old_cout);
    // Restoring stderr
    std::cerr.rdbuf(old_cerr);
  }

  SECTION("Check that incompatible options are detected") {
    // Temporary redirecting stderr in a buffer
    std::stringstream buffer_cerr;
    std::streambuf* old_cerr = std::cerr.rdbuf(buffer_cerr.rdbuf());

    char* argv[] = {command_name, bfs, dfs, filename, nullptr};
    int  argc = static_cast<int>(sizeof(argv) / sizeof(argv[0])) - 1;
    auto out = parse_args_and_compute(argc, argv);

    // Restoring stdout
    std::cout.rdbuf(old_cout);
    // Restoring stderr
    std::cerr.rdbuf(old_cerr);

    std::string str_cout = buffer_cout.str();

    REQUIRE(str_cout.size() == 0);

    std::string str_cerr = buffer_cerr.str();

    REQUIRE(str_cerr.size() > min_help_size);

    REQUIRE(out > 0);
  }
}

#endif  // COMPILE_TESTS
