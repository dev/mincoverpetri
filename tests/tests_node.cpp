/**
 * \file tests_node.cpp
 * \brief Tests for the code in node.hpp and node.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files node.hpp and
 * node.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include "../third_party/catch.hpp"

#include "../src/node.hpp"


TEST_CASE("Node boolean flags manipulation functions", "[node]") {
  SECTION("Node::activate() and Node::deactivate()") {
    Node node(Marking{0});

    node.activate();
    REQUIRE(node.is_active());

    node.deactivate();
    REQUIRE(!node.is_active());
  }

  SECTION("Node::deactivate_subtree()") {
    Node root(Marking{0});
    Node node_1(Marking{0}, &root);
    root.append_p_child(&node_1);
    Node node_2(Marking{0}, &node_1);
    node_1.append_p_child(&node_2);
    Node node_3(Marking{0}, &root);
    root.append_p_child(&node_3);
    Node node_4(Marking{0}, &node_3);
    node_3.append_p_child(&node_4);

    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    SECTION("Deactivate full tree from root") {
      root.deactivate_subtree();

      REQUIRE(!root.is_active());
      REQUIRE(!node_1.is_active());
      REQUIRE(!node_2.is_active());
      REQUIRE(!node_3.is_active());
      REQUIRE(!node_4.is_active());
    }

    SECTION("Deactivate subtree") {
      node_1.deactivate_subtree();

      REQUIRE(root.is_active());
      REQUIRE(!node_1.is_active());
      REQUIRE(!node_2.is_active());
      REQUIRE(node_3.is_active());
      REQUIRE(node_4.is_active());
    }
  }

  SECTION("Node::mark_as_ancestor()") {
    Node node(Marking{0});
    REQUIRE(!node.is_ancestor());
    node.mark_as_ancestor();
    REQUIRE(node.is_ancestor());
  }

  SECTION("Node::mark_ancestors() and Node::unmark_ancestors()") {
    Node root(Marking{0});
    Node node_1(Marking{0}, &root);
    root.append_p_child(&node_1);
    Node node_2(Marking{0}, &node_1);
    node_1.append_p_child(&node_2);
    Node node_3(Marking{0}, &root);
    root.append_p_child(&node_3);
    Node node_4(Marking{0}, &node_3);
    node_3.append_p_child(&node_4);

    SECTION("Marking ancestors from a leaf") {
      node_4.mark_ancestors();

      REQUIRE(root.is_ancestor());
      REQUIRE(!node_1.is_ancestor());
      REQUIRE(!node_2.is_ancestor());
      REQUIRE(node_3.is_ancestor());
      REQUIRE(node_4.is_ancestor());

      node_4.unmark_ancestors();

      REQUIRE(!root.is_ancestor());
      REQUIRE(!node_1.is_ancestor());
      REQUIRE(!node_2.is_ancestor());
      REQUIRE(!node_3.is_ancestor());
      REQUIRE(!node_4.is_ancestor());
    }

    SECTION("Marking ancestors from a node in the middle of the tree") {
      node_1.mark_ancestors();

      REQUIRE(root.is_ancestor());
      REQUIRE(node_1.is_ancestor());
      REQUIRE(!node_2.is_ancestor());
      REQUIRE(!node_3.is_ancestor());
      REQUIRE(!node_4.is_ancestor());

      node_1.unmark_ancestors();

      REQUIRE(!root.is_ancestor());
      REQUIRE(!node_1.is_ancestor());
      REQUIRE(!node_2.is_ancestor());
      REQUIRE(!node_3.is_ancestor());
      REQUIRE(!node_4.is_ancestor());
    }
  }
}

TEST_CASE("Pruning function Node::prune()", "[node]") {
  Node root(Marking{3, 4});
  Node node_1(Marking{3, 2}, &root);
  root.append_p_child(&node_1);
  Node node_2(Marking{3, 1}, &node_1);
  node_1.append_p_child(&node_2);
  Node node_3(Marking{5, 6}, &root);
  root.append_p_child(&node_3);
  Node node_4(Marking{2, 3}, &node_3);
  node_3.append_p_child(&node_4);

  node_2.mark_ancestors();

  SECTION("Example 1") {
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    root.prune(Marking{0, 1});

    REQUIRE(root.is_active());
    REQUIRE(node_1.is_active());
    REQUIRE(node_2.is_active());
    REQUIRE(node_3.is_active());
    REQUIRE(node_4.is_active());
  }

  SECTION("Example 2") {
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    root.prune(Marking{3, 4});

    REQUIRE(!root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(!node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(!node_4.is_active());
  }

  SECTION("Example 3") {
    root.activate();
    node_2.activate();
    node_4.activate();

    root.prune(Marking{4, 2});

    REQUIRE(root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(!node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(node_4.is_active());
  }

  SECTION("Example 4") {
    root.activate();
    node_2.activate();
    node_4.activate();

    root.prune(Marking{2, 3});

    REQUIRE(root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(!node_4.is_active());
  }
}

TEST_CASE("operator<< for Node", "[node]") {
  Node root(Marking{0, 1});
  root.activate();
  Node child_1(Marking{2, 3}, &root);
  root.append_p_child(&child_1);
  Node child_2(Marking{4, 5}, &root);
  root.append_p_child(&child_2);

  std::stringstream stream_root;
  stream_root << root;
  std::stringstream stream_child_1;
  stream_child_1 << child_1;

  std::stringstream ref_root(
      "0xXXXXXXXXXXXX 0 1 (active, children: 0xXXXXXXXXXXXX 0xXXXXXXXXXXXX)");
  std::stringstream ref_child_1(
      "0xXXXXXXXXXXXX 2 3 (inactive, children:)");

  std::string buffer;
  std::vector<std::string> ref_root_elements;
  while (ref_root >> buffer) {
    ref_root_elements.push_back(buffer);
  }
  std::vector<std::string> stream_root_elements;
  while (stream_root >> buffer) {
    stream_root_elements.push_back(buffer);
  }
  std::vector<std::string> ref_child_1_elements;
  while (ref_child_1 >> buffer) {
    ref_child_1_elements.push_back(buffer);
  }
  std::vector<std::string> stream_child_1_elements;
  while (stream_child_1 >> buffer) {
    stream_child_1_elements.push_back(buffer);
  }

  REQUIRE(stream_root_elements.size() == ref_root_elements.size());
  for (std::size_t ind = 1; ind < 5; ++ind) {
    REQUIRE(stream_root_elements[ind] == ref_root_elements[ind]);
  }

  REQUIRE(stream_child_1_elements.size() == ref_child_1_elements.size());
  for (std::size_t ind = 1; ind < 5; ++ind) {
    REQUIRE(stream_child_1_elements[ind] == ref_child_1_elements[ind]);
  }
  REQUIRE(stream_child_1_elements[0] == stream_root_elements[5]);
}

#endif  // COMPILE_TESTS
