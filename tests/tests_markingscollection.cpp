/**
 * \file tests_markingscollection.cpp
 * \brief Tests for the code in markingscollection.hpp and
 *        markingscollection.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files
 * markingscollection.hpp and markingscollection.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include <cstdio>

#include "../third_party/catch.hpp"

#include "../src/markingscollection.hpp"
#include "../src/pnmlreader.hpp"


TEST_CASE("MarkingsCollection::operator==", "[markingscollection]") {
  std::vector<Marking> ref;
  ref.push_back(Marking{0, 0, 1});
  ref.push_back(Marking{0, 2, 0});
  ref.push_back(Marking{Marking::omega, 0, 0});
  MarkingsCollection mark_collec_ref(std::move(ref));

  SECTION("Same data, same order") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 0, 1});
    test.push_back(Marking{0, 2, 0});
    test.push_back(Marking{Marking::omega, 0, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref == mark_collec_test);
  }

  SECTION("Same data, different order") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 2, 0});
    test.push_back(Marking{Marking::omega, 0, 0});
    test.push_back(Marking{0, 0, 1});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref == mark_collec_test);
  }

  SECTION("More data") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 0, 1});
    test.push_back(Marking{0, 2, 0});
    test.push_back(Marking{1, 0, 1});
    test.push_back(Marking{Marking::omega, 0, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref != mark_collec_test);
  }
  SECTION("Less data") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 0, 1});
    test.push_back(Marking{0, 2, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref != mark_collec_test);
  }

  SECTION("Different data") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 0, 3});
    test.push_back(Marking{0, 2, 0});
    test.push_back(Marking{Marking::omega, 0, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref != mark_collec_test);
  }

  SECTION("Shorter markings") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 1});
    test.push_back(Marking{2, 0});
    test.push_back(Marking{0, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref != mark_collec_test);
  }

  SECTION("Longer markings") {
    std::vector<Marking> test;
    test.push_back(Marking{0, 0, 0, 1});
    test.push_back(Marking{0, 0, 2, 0});
    test.push_back(Marking{0, Marking::omega, 0, 0});
    MarkingsCollection mark_collec_test(std::move(test));

    REQUIRE(mark_collec_ref != mark_collec_test);
  }
}

TEST_CASE("MarkingsCollection::read()", "[markingscollection]") {
  std::vector<Marking> ref;
  ref.push_back(Marking{Marking::omega, 1, 1, 0, 0});
  ref.push_back(Marking{Marking::omega, 1, 0, 1, 0});
  ref.push_back(Marking{Marking::omega, 0, 1, 0, 1});
  MarkingsCollection mark_collec_ref(std::move(ref));

  std::string filename(TEST_DATA_PATH "03_solution.xml");
  MarkingsCollection mark_collec;
  mark_collec.read(filename);

  REQUIRE(mark_collec_ref == mark_collec);

  ref.resize(0);
  ref.push_back(Marking{0, 1, 33, Marking::omega, 0});
  ref.push_back(Marking{Marking::omega, 1, 102, 1, 0});
  MarkingsCollection mark_collec_ref_bis(std::move(ref));

  filename = TEST_DATA_PATH "08_solution.xml";
  mark_collec.read(filename);

  REQUIRE(mark_collec_ref_bis == mark_collec);
}

TEST_CASE("MarkingsCollection::write()", "[markingscollection]") {
  std::string filename(TEST_DATA_PATH "03_tmp_test.xml");

  std::vector<Marking> ref;
  ref.push_back(Marking{Marking::omega, 1, 1, 0, 0});
  ref.push_back(Marking{Marking::omega, 1, 0, 1, 0});
  ref.push_back(Marking{Marking::omega, 0, 1, 0, 1});
  MarkingsCollection mark_collec_ref(std::move(ref));

  mark_collec_ref.write(filename);

  MarkingsCollection mark_collec;
  mark_collec.read(filename);

  std::remove(filename.c_str());

  REQUIRE(mark_collec_ref == mark_collec);
}

TEST_CASE("MarkingsCollection::is_antichain()", "[markingscollection]") {
  SECTION("Example 1") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(mark_collec.is_antichain());
  }

  SECTION("Example 2") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 0, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_antichain());
  }

  SECTION("Example 3") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 0, Marking::omega});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_antichain());
  }

  SECTION("Example 4") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 2, 0});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(mark_collec.is_antichain());
  }

  SECTION("Example 5") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 2, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_antichain());
  }

  SECTION("Example 6") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 2, 0});
    vec.push_back(Marking{4, 1, 0});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(mark_collec.is_antichain());
  }

  SECTION("Example 7") {
    std::vector<Marking> vec;
    vec.push_back(Marking{0, 0, 1});
    vec.push_back(Marking{0, 2, 0});
    vec.push_back(Marking{4, 2, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_antichain());
  }
}

TEST_CASE("MarkingsCollection::is_covering_set()",
    "[markingscollection]") {
  PnmlReader reader;
  std::string filename = TEST_DATA_PATH "03.pnml";
  auto net = reader.read(filename);

  SECTION("Using the minimal covering set") {
    std::vector<Marking> vec;
    vec.push_back(Marking{Marking::omega, 1, 1, 0, 0});
    vec.push_back(Marking{Marking::omega, 1, 0, 1, 0});
    vec.push_back(Marking{Marking::omega, 0, 1, 0, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(mark_collec.is_covering_set(net));
  }

  SECTION("Using a non minimal covering set") {
    std::vector<Marking> vec;
    vec.push_back(Marking{Marking::omega, 1, 1, 0, 0});
    vec.push_back(Marking{Marking::omega, 1, 0, 1, 0});
    vec.push_back(Marking{Marking::omega, 0, 1, 0, 1});
    vec.push_back(Marking{1, 0, 0, 0, 0});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(mark_collec.is_covering_set(net));
  }

  SECTION("Using a non covering set, example 1") {
    std::vector<Marking> vec;
    vec.push_back(Marking{Marking::omega, 1, 1, 0, 0});
    vec.push_back(Marking{Marking::omega, 1, 0, 1, 0});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_covering_set(net));
  }

  SECTION("Using a non covering set, example 2") {
    std::vector<Marking> vec;
    vec.push_back(Marking{1, 1, 1, 0, 0});
    vec.push_back(Marking{Marking::omega, 1, 0, 1, 0});
    vec.push_back(Marking{Marking::omega, 0, 1, 0, 1});
    MarkingsCollection mark_collec(std::move(vec));

    REQUIRE(!mark_collec.is_covering_set(net));
  }
}

#endif  // COMPILE_TESTS
