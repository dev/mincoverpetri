/**
 * \file tests_orderedtuples.cpp
 * \brief Tests for the code in orderedtuples.hpp and orderedtuples.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files orderedtuples.hpp and
 * orderedtuples.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include "../third_party/catch.hpp"

#include "../src/orderedtuples.hpp"


TEST_CASE("OrderedTuples::is_dominated()", "[orderedtuples]") {
  // NOTE: The test of this function is related to the test of is_dominated()
  // that it can replace

  OrderedTuples ord_tuples;

  REQUIRE(!ord_tuples.is_dominated(Marking{0, 2, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{1, 2, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{1, 2, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{1, 3, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{2, 0, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 3, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 4, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 4}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 6}));
  REQUIRE(!ord_tuples.is_dominated(Marking{Marking::omega, 0, 0}));

  Node root(Marking{1, 2, 3});
  root.activate();
  ord_tuples.insert(&root);

  REQUIRE(ord_tuples.is_dominated(Marking{0, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{1, 3, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{2, 0, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 3, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 4, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 4}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 6}));
  REQUIRE(!ord_tuples.is_dominated(Marking{Marking::omega, 0, 0}));

  Node node_1(Marking{1, 3, 3}, &root);
  root.append_p_child(&node_1);
  node_1.activate();
  ord_tuples.insert(&node_1);

  REQUIRE(ord_tuples.is_dominated(Marking{0, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 3}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 3, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{2, 0, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 3, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 4, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 4}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 6}));
  REQUIRE(!ord_tuples.is_dominated(Marking{Marking::omega, 0, 0}));

  Node node_2(Marking{0, 4, 0}, &node_1);
  node_1.append_p_child(&node_2);
  node_2.activate();
  ord_tuples.insert(&node_2);

  REQUIRE(ord_tuples.is_dominated(Marking{0, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 3}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 3, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{2, 0, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 3, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 4, 0}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 4}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 6}));
  REQUIRE(!ord_tuples.is_dominated(Marking{Marking::omega, 0, 0}));

  Node node_3(Marking{0, 4, 5}, &root);
  root.append_p_child(&node_3);
  node_3.activate();
  ord_tuples.insert(&node_3);

  REQUIRE(ord_tuples.is_dominated(Marking{0, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 2, 3}));
  REQUIRE(ord_tuples.is_dominated(Marking{1, 3, 3}));
  REQUIRE(!ord_tuples.is_dominated(Marking{2, 0, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 3, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 4, 0}));
  REQUIRE(ord_tuples.is_dominated(Marking{0, 0, 4}));
  REQUIRE(!ord_tuples.is_dominated(Marking{0, 0, 6}));
  REQUIRE(!ord_tuples.is_dominated(Marking{Marking::omega, 0, 0}));
}

TEST_CASE("OrderedTuples::prune()", "[orderedtuples]") {
  // NOTE: The test of this function is related to the test of
  // Node::prune() that it can replace

  Node root(Marking{3, 4});
  Node node_1(Marking{3, 2}, &root);
  root.append_p_child(&node_1);
  Node node_2(Marking{3, 1}, &node_1);
  node_1.append_p_child(&node_2);
  Node node_3(Marking{5, 6}, &root);
  root.append_p_child(&node_3);
  Node node_4(Marking{2, 3}, &node_3);
  node_3.append_p_child(&node_4);

  node_2.mark_ancestors();

  OrderedTuples ord_tuples;
  ord_tuples.insert(&root);
  ord_tuples.insert(&node_1);
  ord_tuples.insert(&node_2);
  ord_tuples.insert(&node_3);
  ord_tuples.insert(&node_4);

  SECTION("Example 1") {
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    ord_tuples.prune(Marking{0, 1});

    REQUIRE(root.is_active());
    REQUIRE(node_1.is_active());
    REQUIRE(node_2.is_active());
    REQUIRE(node_3.is_active());
    REQUIRE(node_4.is_active());
  }

  SECTION("Example 2") {
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    ord_tuples.prune(Marking{3, 4});

    REQUIRE(!root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(!node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(!node_4.is_active());
  }

  SECTION("Example 3") {
    root.activate();
    node_2.activate();
    node_4.activate();

    ord_tuples.prune(Marking{4, 2});

    REQUIRE(root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(!node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(node_4.is_active());
  }

  SECTION("Example 4") {
    root.activate();
    node_2.activate();
    node_4.activate();

    ord_tuples.prune(Marking{2, 3});

    REQUIRE(root.is_active());
    REQUIRE(!node_1.is_active());
    REQUIRE(node_2.is_active());
    REQUIRE(!node_3.is_active());
    REQUIRE(!node_4.is_active());
  }
}

TEST_CASE("OrderedTuples::accelerate()", "[orderedtuples]") {
  // NOTE: The test of this function is related to the test of accelerate()
  // that it can replace.
  // Note that the results of the two accelerate() functions are not exactly the
  // same, see comments in the code of OrderedTuplesNode::accelerate() for
  // details.
  // The difference can be noticed in one of the examples below
  // (see related comment below).

  OrderedTuples ord_tuples;

  Node root(Marking{0, 1, 2});
  Node node_1(Marking{1, 1, 1}, &root);
  root.append_p_child(&node_1);
  Node node_2(Marking{1, 4, 2}, &node_1);
  node_1.append_p_child(&node_2);
  Node node_3(Marking{0, 4, 5}, &root);
  root.append_p_child(&node_3);
  Node node_4(Marking{3, 2, 1}, &node_3);
  node_3.append_p_child(&node_4);

  ord_tuples.insert(&root);
  ord_tuples.insert(&node_1);
  ord_tuples.insert(&node_2);
  ord_tuples.insert(&node_3);
  ord_tuples.insert(&node_4);

  SECTION("All nodes active") {
    Marking marking;
    Marking expected_marking;
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    node_2.mark_ancestors();

    marking =  Marking{0, 1, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking =  Marking{0, 1, 4};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking =  Marking{2, 2, 1};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, 1};
    REQUIRE(marking == expected_marking);

    marking =  Marking{1, 2, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking =  Marking{4, 3, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    node_2.unmark_ancestors();
    node_4.mark_ancestors();

    marking =  Marking{0, 1, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking =  Marking{0, 1, 4};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking =  Marking{2, 2, 1};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{2, 2, 1};
    REQUIRE(marking == expected_marking);

    // NOTE: For the following example, the expected result is different
    // from the one obtained using the other accelerate() function.
    marking =  Marking{1, 2, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking =  Marking{4, 3, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);
  }

  SECTION("All nodes active except root") {
    Marking marking;
    Marking expected_marking;
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    node_2.mark_ancestors();

    marking =  Marking{0, 1, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking =  Marking{0, 1, 4};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 4};
    REQUIRE(marking == expected_marking);

    marking =  Marking{2, 2, 1};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, 1};
    REQUIRE(marking == expected_marking);

    marking =  Marking{1, 2, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{1, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking =  Marking{4, 3, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    node_2.unmark_ancestors();
    node_4.mark_ancestors();

    marking =  Marking{0, 1, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking =  Marking{0, 1, 4};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{0, 1, 4};
    REQUIRE(marking == expected_marking);

    marking =  Marking{2, 2, 1};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{2, 2, 1};
    REQUIRE(marking == expected_marking);

    marking =  Marking{1, 2, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{1, 2, 2};
    REQUIRE(marking == expected_marking);

    marking =  Marking{4, 3, 2};
    ord_tuples.accelerate(marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);
  }
}

#endif  // COMPILE_TESTS
