/**
 * \file tests_petrinet.cpp
 * \brief Tests for the code in petrinet.hpp and petrinet.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files petrinet.hpp and
 * petrinet.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include "../third_party/catch.hpp"

#include "../src/petrinet.hpp"


TEST_CASE("Move constructor") {
  PetriNet net;
  net.add_place(Place{"p0", 0});
  net.add_transition(Transition{"t0"});
  net.add_arc_t_p(ArcTP{"t0_p0", net.transitions()[0], net.places()[0], 1});
  net.add_arc_p_t(ArcPT{"p0_t0", net.places()[0], net.transitions()[0], 1});

  PetriNet moved(std::move(net));

  REQUIRE(moved.places().size() == 1);
  REQUIRE(moved.transitions().size() == 1);
  REQUIRE(moved.arcs_t_p().size() == 1);
  REQUIRE(moved.arcs_p_t().size() == 1);
}


TEST_CASE("Testing PetriNet member functions and operator<<", "[petrinet]") {
  PetriNet petri_net;
  Marking expected_marking;

  REQUIRE(petri_net.places().size() == 0);
  REQUIRE(petri_net.transitions().size() == 0);
  REQUIRE(petri_net.arcs_p_t().size() == 0);
  REQUIRE(petri_net.arcs_t_p().size() == 0);
  REQUIRE(petri_net.initial_marking().size() == 0);
  REQUIRE(petri_net.mapping_p_t().size() == 0);
  REQUIRE(petri_net.mapping_t_p().size() == 0);
  REQUIRE(petri_net.mapping_diff().size() == 0);

  petri_net.add_place(Place{"p0", 0});

  REQUIRE(petri_net.places().size() == 1);
  REQUIRE(petri_net.transitions().size() == 0);
  REQUIRE(petri_net.arcs_p_t().size() == 0);
  REQUIRE(petri_net.arcs_t_p().size() == 0);
  REQUIRE(petri_net.initial_marking().size() == 1);
  REQUIRE(petri_net.mapping_p_t().size() == 0);
  REQUIRE(petri_net.mapping_t_p().size() == 0);
  REQUIRE(petri_net.mapping_diff().size() == 0);
  REQUIRE(petri_net.places()[0].id == "p0");
  expected_marking = Marking{0};
  REQUIRE(petri_net.initial_marking() == expected_marking);

  petri_net.add_transition(Transition{"t0"});

  REQUIRE(petri_net.places().size() == 1);
  REQUIRE(petri_net.transitions().size() == 1);
  REQUIRE(petri_net.arcs_p_t().size() == 0);
  REQUIRE(petri_net.arcs_t_p().size() == 0);
  REQUIRE(petri_net.initial_marking().size() == 1);
  REQUIRE(petri_net.mapping_p_t().size() == 1);
  REQUIRE(petri_net.mapping_t_p().size() == 1);
  REQUIRE(petri_net.mapping_diff().size() == 1);

  REQUIRE(petri_net.places()[0].id == "p0");
  REQUIRE(petri_net.initial_marking() == Marking{0});
  REQUIRE(petri_net.transitions()[0].id == "t0");

  expected_marking = Marking{0};
  REQUIRE(petri_net.mapping_p_t()[0] == expected_marking);
  REQUIRE(petri_net.mapping_t_p()[0] == expected_marking);
  REQUIRE(petri_net.mapping_diff()[0] == expected_marking);

  petri_net.add_arc_p_t(ArcPT{"p0_t0", petri_net.places()[0],
      petri_net.transitions()[0], 1});

  REQUIRE(petri_net.places().size() == 1);
  REQUIRE(petri_net.transitions().size() == 1);
  REQUIRE(petri_net.arcs_p_t().size() == 1);
  REQUIRE(petri_net.arcs_t_p().size() == 0);
  REQUIRE(petri_net.initial_marking().size() == 1);
  REQUIRE(petri_net.mapping_p_t().size() == 1);
  REQUIRE(petri_net.mapping_t_p().size() == 1);
  REQUIRE(petri_net.mapping_diff().size() == 1);

  REQUIRE(petri_net.places()[0].id == "p0");
  expected_marking = Marking{0};
  REQUIRE(petri_net.initial_marking() == expected_marking);
  REQUIRE(petri_net.transitions()[0].id == "t0");
  REQUIRE(petri_net.mapping_t_p()[0] == expected_marking);
  expected_marking = Marking{1};
  REQUIRE(petri_net.mapping_p_t()[0] == expected_marking);
  expected_marking = Marking{-1};
  REQUIRE(petri_net.mapping_diff()[0] == expected_marking);

  petri_net.add_arc_t_p(ArcTP{"t0_p0", petri_net.transitions()[0],
      petri_net.places()[0], 11});

  REQUIRE(petri_net.places().size() == 1);
  REQUIRE(petri_net.transitions().size() == 1);
  REQUIRE(petri_net.arcs_p_t().size() == 1);
  REQUIRE(petri_net.arcs_t_p().size() == 1);
  REQUIRE(petri_net.initial_marking().size() == 1);
  REQUIRE(petri_net.mapping_p_t().size() == 1);
  REQUIRE(petri_net.mapping_t_p().size() == 1);
  REQUIRE(petri_net.mapping_diff().size() == 1);

  REQUIRE(petri_net.places()[0].id == "p0");
  expected_marking = Marking{0};
  REQUIRE(petri_net.initial_marking() == expected_marking);
  REQUIRE(petri_net.transitions()[0].id == "t0");
  expected_marking = Marking{1};
  REQUIRE(petri_net.mapping_p_t()[0] == expected_marking);
  expected_marking = Marking{11};
  REQUIRE(petri_net.mapping_t_p()[0] == expected_marking);
  expected_marking = Marking{10};
  REQUIRE(petri_net.mapping_diff()[0] == expected_marking);

  petri_net.add_place(Place{"p1", 1});
  petri_net.add_place(Place{"p2", 2});
  petri_net.add_transition(Transition{"t1"});
  petri_net.add_arc_p_t(ArcPT{"p2_t0", petri_net.places()[2],
        petri_net.transitions()[0], 2});
  petri_net.add_arc_p_t(ArcPT{"p1_t1", petri_net.places()[1],
        petri_net.transitions()[1], 3});
  petri_net.add_arc_t_p(ArcTP{"t1_p1", petri_net.transitions()[1],
        petri_net.places()[1], 12});

  // NOTE: Due to some limitations of the testing framework, when executed
  // inside REQUIRE(), the following line creates an unexpected exception.
  // Running the command beforehand seems to solve the problem.
  petri_net.mapping_p_t().size();

  REQUIRE(petri_net.places().size() == 3);
  REQUIRE(petri_net.transitions().size() == 2);
  REQUIRE(petri_net.arcs_p_t().size() == 3);
  REQUIRE(petri_net.arcs_t_p().size() == 2);
  REQUIRE(petri_net.initial_marking().size() == 3);
  REQUIRE(petri_net.mapping_p_t().size() == 2);
  REQUIRE(petri_net.mapping_t_p().size() == 2);
  REQUIRE(petri_net.mapping_diff().size() == 2);

  REQUIRE(petri_net.places()[0].id == "p0");
  REQUIRE(petri_net.places()[1].id == "p1");
  REQUIRE(petri_net.places()[2].id == "p2");
  expected_marking = Marking{0, 1, 2};
  REQUIRE(petri_net.initial_marking() == expected_marking);
  REQUIRE(petri_net.transitions()[0].id == "t0");
  REQUIRE(petri_net.transitions()[1].id == "t1");
  expected_marking = Marking{1, 0, 2};
  REQUIRE(petri_net.mapping_p_t()[0] == expected_marking);
  expected_marking = Marking{0, 3, 0};
  REQUIRE(petri_net.mapping_p_t()[1] == expected_marking);
  expected_marking = Marking{11, 0, 0};
  REQUIRE(petri_net.mapping_t_p()[0] == expected_marking);
  expected_marking = Marking{0, 12, 0};
  REQUIRE(petri_net.mapping_t_p()[1] == expected_marking);
  expected_marking = Marking{10, 0, -2};
  REQUIRE(petri_net.mapping_diff()[0] == expected_marking);
  expected_marking = Marking{0, 9, 0};
  REQUIRE(petri_net.mapping_diff()[1] == expected_marking);

  std::string ref = "Places:\n"
      "id: p0, init. mark.: 0\n"
      "id: p1, init. mark.: 1\n"
      "id: p2, init. mark.: 2\n"
      "Transitions:\n"
      "id: t0\n"
      "id: t1\n"
      "Arcs places->transitions:\n"
      "id: p0_t0, source: p0 -> target: t0, value: 1\n"
      "id: p2_t0, source: p2 -> target: t0, value: 2\n"
      "id: p1_t1, source: p1 -> target: t1, value: 3\n"
      "Arcs transitions->places:\n"
      "id: t0_p0, source: t0 -> target: p0, value: 11\n"
      "id: t1_p1, source: t1 -> target: p1, value: 12\n";
  std::ostringstream stream;
  stream << petri_net;
  REQUIRE(stream.str() == ref);
}

#endif  // COMPILE_TESTS
