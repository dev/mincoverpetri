/**
 * \file tests_mincoversetcomputer.cpp
 * \brief Tests for the code in mincoversetcomputer.hpp and
 *        mincoversetcomputer.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files
 * mincoversetcomputer.hpp and mincoversetcomputer.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include <bitset>

#include "../third_party/catch.hpp"

#include "../src/mincoversetcomputer.hpp"
#include "../src/pnmlreader.hpp"


TEST_CASE("MinCoverSetComputer::compute() and "
    "MinCoverSetComputer::computation_stats()", "[mincoversetcomputer]") {
  std::vector<std::string> filenames{
    TEST_DATA_PATH "00.pnml", TEST_DATA_PATH "03.pnml",
    TEST_DATA_PATH "04.pnml", TEST_DATA_PATH "05.pnml",
    TEST_DATA_PATH "06.pnml", TEST_DATA_PATH "07.pnml"};

  std::vector<ComputationStats> stats_bfs = {
      ComputationStats{0, 0, 1, 1, -1}, ComputationStats{4, 4, 3, 3, -1},
      ComputationStats{7, 7, 5, 1, -1}, ComputationStats{75, 75, 41, 41, -1},
      ComputationStats{29, 22, 7, 1, -1}, ComputationStats{11, 10, 9, 6, -1}};
  std::vector<ComputationStats> stats_dfs = {
      ComputationStats{0, 0, 1, 1, -1}, ComputationStats{4, 4, 3, 3, -1},
      ComputationStats{7, 7, 5, 1, -1}, ComputationStats{75, 75, 41, 41, -1},
      ComputationStats{29, 19, 7, 1, -1}, ComputationStats{15, 15, 13, 6, -1}};

  int ind_file = 0;
  for (const auto& filename : filenames) {
    PnmlReader reader;
    auto net = reader.read(filename);

    MarkingsCollection min_cover_set_ref;
    auto solution_filename = filename.substr(0, filename.size()-5)
        + "_solution.xml";
    min_cover_set_ref.read(solution_filename);

    MinCoverSetComputer min_cover_set_computer_reused(net);

    for (int mask = 0; mask != (1<<4); ++mask) {
      // Using bitset to test all possible combinations of boolean flags
      // passed to mincoverset::compute()
      std::bitset<4> flags(mask);
      MonotonePruningParam param{flags[0], flags[1], flags[2], flags[3]};

      auto min_cover_set_reused = min_cover_set_computer_reused.compute(param);

      REQUIRE(min_cover_set_ref == min_cover_set_reused);

      MinCoverSetComputer min_cover_set_computer_fresh(net);
      auto min_cover_set_fresh = min_cover_set_computer_fresh.compute(param);

      REQUIRE(min_cover_set_ref == min_cover_set_fresh);

      auto param_reused = min_cover_set_computer_reused.computation_stats();
      auto param_fresh = min_cover_set_computer_fresh.computation_stats();
      if (param.bfs_traversal) {
        REQUIRE(param_reused.nb_wait == stats_bfs[ind_file].nb_wait);
        REQUIRE(param_reused.nb_nodes == stats_bfs[ind_file].nb_nodes);
        REQUIRE(param_reused.nb_min_cover_set ==
            stats_bfs[ind_file].nb_min_cover_set);

        REQUIRE(param_fresh.nb_wait == stats_bfs[ind_file].nb_wait);
        REQUIRE(param_fresh.nb_nodes == stats_bfs[ind_file].nb_nodes);
        REQUIRE(param_fresh.nb_min_cover_set ==
            stats_bfs[ind_file].nb_min_cover_set);
      } else {
        REQUIRE(param_reused.nb_wait == stats_dfs[ind_file].nb_wait);
        REQUIRE(param_reused.nb_nodes == stats_dfs[ind_file].nb_nodes);
        REQUIRE(param_reused.nb_min_cover_set ==
            stats_dfs[ind_file].nb_min_cover_set);

        REQUIRE(param_fresh.nb_wait == stats_dfs[ind_file].nb_wait);
        REQUIRE(param_fresh.nb_nodes == stats_dfs[ind_file].nb_nodes);
        REQUIRE(param_fresh.nb_min_cover_set ==
            stats_dfs[ind_file].nb_min_cover_set);
      }
      REQUIRE(param_reused.computation_time > 0.);
      REQUIRE(param_fresh.computation_time > 0.);
    }
    ++ind_file;
  }
}

TEST_CASE("MinCoverSetComputer::is_dominated()", "[mincoversetcomputer]") {
  Node root(Marking{1, 2, 3});
  root.activate();

  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 3}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{1, 3, 3}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{2, 0, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 3, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 4, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 4}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 6}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{Marking::omega, 0, 0},
      &root));

  Node node_1(Marking{1, 3, 3}, &root);
  root.append_p_child(&node_1);
  node_1.activate();

  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 3}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 3, 3}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{2, 0, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 3, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 4, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 4}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 6}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{Marking::omega, 0, 0},
      &root));

  Node node_2(Marking{0, 4, 0}, &node_1);
  node_1.append_p_child(&node_2);
  node_2.activate();

  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 3}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 3, 3}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{2, 0, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 3, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 4, 0}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 4}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 6}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{Marking::omega, 0, 0},
      &root));

  Node node_3(Marking{0, 4, 5}, &root);
  root.append_p_child(&node_3);
  node_3.activate();

  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 2, 3}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{1, 3, 3}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{2, 0, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 3, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 4, 0}, &root));
  REQUIRE(MinCoverSetComputer::is_dominated(Marking{0, 0, 4}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{0, 0, 6}, &root));
  REQUIRE(!MinCoverSetComputer::is_dominated(Marking{Marking::omega, 0, 0},
      &root));
}

TEST_CASE("MinCoverSetComputer::accelerate()", "[mincoversetcomputer]") {
  Node root(Marking{0, 1, 2});
  Node node_1(Marking{1, 1, 1}, &root);
  root.append_p_child(&node_1);
  Node node_2(Marking{1, 4, 2}, &node_1);
  node_1.append_p_child(&node_2);
  Node node_3(Marking{0, 4, 5}, &root);
  root.append_p_child(&node_3);
  Node node_4(Marking{3, 2, 1}, &node_3);
  node_3.append_p_child(&node_4);

  SECTION("All nodes active") {
    Marking marking;
    Marking expected_marking;
    root.activate();
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    marking = Marking{0, 1, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 4};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{0, 1, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{2, 2, 1};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, 1};
    REQUIRE(marking == expected_marking);

    marking = Marking{1, 2, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{4, 3, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 4};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{0, 1, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{2, 2, 1};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{2, 2, 1};
    REQUIRE(marking == expected_marking);

    marking = Marking{1, 2, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{4, 3, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);
  }

  SECTION("All nodes active except root") {
    Marking marking;
    Marking expected_marking;
    node_1.activate();
    node_2.activate();
    node_3.activate();
    node_4.activate();

    marking = Marking{0, 1, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 4};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{0, 1, 4};
    REQUIRE(marking == expected_marking);

    marking = Marking{2, 2, 1};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, 1};
    REQUIRE(marking == expected_marking);

    marking = Marking{1, 2, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{1, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{4, 3, 2};
    MinCoverSetComputer::accelerate(&node_2, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{0, 1, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{0, 1, 4};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{0, 1, 4};
    REQUIRE(marking == expected_marking);

    marking = Marking{2, 2, 1};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{2, 2, 1};
    REQUIRE(marking == expected_marking);

    marking = Marking{1, 2, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{1, 2, 2};
    REQUIRE(marking == expected_marking);

    marking = Marking{4, 3, 2};
    MinCoverSetComputer::accelerate(&node_4, marking);
    expected_marking =  Marking{Marking::omega, Marking::omega, Marking::omega};
    REQUIRE(marking == expected_marking);
  }
}

#endif  // COMPILE_TESTS
