/**
 * \file tests_marking.cpp
 * \brief Tests for the code in marking.hpp and marking.cpp
 * \author Florent JAILLET
 * \date 2016-2018
 * \copyright Université d'Aix Marseille (AMU) -
 *            Centre National de la Recherche Scientifique (CNRS) -
 *            Université de Toulon (UT).
 *            Laboratoire d'Informatique et Systèmes - UMR 7020
 *            This file is released under the GPLv3+.
 *
 * This files defines the test cases for the code in files marking.hpp and
 * marking.cpp.
 *
 */

/* This file is part of MinCoverPetri.
 *
 * MinCoverPetri is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MinCoverPetri is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinCoverPetri. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef COMPILE_TESTS

#include "../third_party/catch.hpp"

#include "../src/marking.hpp"


TEST_CASE("Marking::operator==", "[marking]") {
  SECTION("Markings of size 1") {
    Marking marking_0{0};
    Marking marking_1{1};
    Marking marking_2{2};
    Marking marking_2_alt{2};

    REQUIRE(marking_0 == marking_0);
    REQUIRE(!(marking_0 == marking_1));
    REQUIRE(!(marking_0 == marking_2));

    REQUIRE(!(marking_1 == marking_0));
    REQUIRE(marking_1 == marking_1);
    REQUIRE(!(marking_1 == marking_2));

    REQUIRE(!(marking_2 == marking_0));
    REQUIRE(!(marking_2 == marking_1));
    REQUIRE(marking_2 == marking_2);
    REQUIRE(marking_2 == marking_2_alt);
  }

  SECTION("Markings of size 3") {
    Marking marking_000{0, 0, 0};
    Marking marking_001{0, 0, 1};
    Marking marking_010{0, 1, 0};
    Marking marking_100{1, 0, 0};
    Marking marking_110{1, 1, 0};
    Marking marking_111{1, 1, 1};
    Marking marking_222{2, 2, 2};
    Marking marking_222_alt{2, 2, 2};

    REQUIRE(marking_000 == marking_000);
    REQUIRE(!(marking_000 == marking_001));
    REQUIRE(!(marking_001 == marking_000));
    REQUIRE(!(marking_000 == marking_010));
    REQUIRE(!(marking_010 == marking_000));
    REQUIRE(!(marking_000 == marking_100));
    REQUIRE(!(marking_100 == marking_000));
    REQUIRE(!(marking_100 == marking_110));
    REQUIRE(!(marking_110 == marking_100));
    REQUIRE(!(marking_000 == marking_111));
    REQUIRE(!(marking_111 == marking_000));

    REQUIRE(!(marking_111 == marking_222));
    REQUIRE(!(marking_222 == marking_111));

    REQUIRE(marking_111 == marking_111);
    REQUIRE(marking_222 == marking_222_alt);
  }
}


TEST_CASE("Marking::operator!=", "[marking]") {
  SECTION("Markings of size 1") {
    Marking marking_0{0};
    Marking marking_1{1};
    Marking marking_2{2};
    Marking marking_2_alt{2};

    REQUIRE(!(marking_0 != marking_0));
    REQUIRE(marking_0 != marking_1);
    REQUIRE(marking_0 != marking_2);

    REQUIRE(marking_1 != marking_0);
    REQUIRE(marking_1 != marking_2);

    REQUIRE(marking_2 != marking_0);
    REQUIRE(marking_2 != marking_1);
    REQUIRE(!(marking_2 != marking_2));
    REQUIRE(!(marking_2 != marking_2_alt));
  }

  SECTION("Markings of size 3") {
    Marking marking_000{0, 0, 0};
    Marking marking_001{0, 0, 1};
    Marking marking_010{0, 1, 0};
    Marking marking_100{1, 0, 0};
    Marking marking_110{1, 1, 0};
    Marking marking_111{1, 1, 1};
    Marking marking_222{2, 2, 2};
    Marking marking_222_alt{2, 2, 2};

    REQUIRE(!(marking_000 != marking_000));
    REQUIRE(marking_000 != marking_001);
    REQUIRE(marking_001 != marking_000);
    REQUIRE(marking_000 != marking_010);
    REQUIRE(marking_010 != marking_000);
    REQUIRE(marking_000 != marking_100);
    REQUIRE(marking_100 != marking_000);
    REQUIRE(marking_100 != marking_110);
    REQUIRE(marking_110 != marking_100);
    REQUIRE(marking_000 != marking_111);
    REQUIRE(marking_111 != marking_000);

    REQUIRE(marking_111 != marking_222);
    REQUIRE(marking_222 != marking_111);

    REQUIRE(!(marking_111 != marking_111));
    REQUIRE(!(marking_222 != marking_222_alt));
  }
}

TEST_CASE("Marking::operator<=", "[marking]") {
  SECTION("Markings of size 1") {
    Marking marking_0{0};
    Marking marking_1{1};
    Marking marking_2{2};
    Marking marking_2_alt{2};

    REQUIRE(marking_0 <= marking_0);
    REQUIRE(marking_0 <= marking_1);
    REQUIRE(marking_0 <= marking_2);

    REQUIRE(marking_1 <= marking_1);
    REQUIRE(marking_1 <= marking_2);

    REQUIRE(marking_2 <= marking_2);
    REQUIRE(marking_2 <= marking_2_alt);

    REQUIRE(!(marking_1 <= marking_0));
    REQUIRE(!(marking_2 <= marking_0));
    REQUIRE(!(marking_2 <= marking_1));
  }

  SECTION("Markings of size 3") {
    Marking marking_000{0, 0, 0};
    Marking marking_001{0, 0, 1};
    Marking marking_010{0, 1, 0};
    Marking marking_100{1, 0, 0};
    Marking marking_110{1, 1, 0};
    Marking marking_101{1, 0, 1};
    Marking marking_111{1, 1, 1};
    Marking marking_120{1, 2, 0};
    Marking marking_200{2, 0, 0};
    Marking marking_222{2, 2, 2};
    Marking marking_222_alt{2, 2, 2};

    REQUIRE(marking_000 <= marking_000);
    REQUIRE(marking_000 <= marking_001);
    REQUIRE(marking_000 <= marking_010);
    REQUIRE(marking_000 <= marking_100);
    REQUIRE(marking_000 <= marking_110);
    REQUIRE(marking_000 <= marking_101);
    REQUIRE(marking_000 <= marking_111);

    REQUIRE(marking_100 <= marking_200);
    REQUIRE(marking_110 <= marking_120);
    REQUIRE(marking_111 <= marking_222);

    REQUIRE(marking_120 <= marking_120);
    REQUIRE(marking_222 <= marking_222_alt);

    REQUIRE(!(marking_001 <= marking_000));
    REQUIRE(!(marking_010 <= marking_000));
    REQUIRE(!(marking_100 <= marking_000));
    REQUIRE(!(marking_110 <= marking_000));
    REQUIRE(!(marking_101 <= marking_000));
    REQUIRE(!(marking_111 <= marking_000));

    REQUIRE(!(marking_200 <= marking_100));
    REQUIRE(!(marking_120 <= marking_110));
    REQUIRE(!(marking_222 <= marking_111));

    REQUIRE(!(marking_100 <= marking_010));
    REQUIRE(!(marking_010 <= marking_100));
    REQUIRE(!(marking_010 <= marking_001));
    REQUIRE(!(marking_001 <= marking_010));
    REQUIRE(!(marking_120 <= marking_200));
    REQUIRE(!(marking_200 <= marking_120));
  }
}

TEST_CASE("Marking::operator<", "[marking]") {
  SECTION("Markings of size 1") {
    Marking marking_0{0};
    Marking marking_1{1};
    Marking marking_2{2};
    Marking marking_2_alt{2};

    REQUIRE(!(marking_0 < marking_0));
    REQUIRE(marking_0 < marking_1);
    REQUIRE(marking_0 < marking_2);

    REQUIRE(!(marking_1 < marking_1));
    REQUIRE(marking_1 < marking_2);

    REQUIRE(!(marking_2 < marking_2));
    REQUIRE(!(marking_2 < marking_2_alt));

    REQUIRE(!(marking_1 < marking_0));
    REQUIRE(!(marking_2 < marking_0));
    REQUIRE(!(marking_2 < marking_1));
  }

  SECTION("Markings of size 3") {
    Marking marking_000{0, 0, 0};
    Marking marking_001{0, 0, 1};
    Marking marking_010{0, 1, 0};
    Marking marking_100{1, 0, 0};
    Marking marking_110{1, 1, 0};
    Marking marking_101{1, 0, 1};
    Marking marking_111{1, 1, 1};
    Marking marking_120{1, 2, 0};
    Marking marking_200{2, 0, 0};
    Marking marking_222{2, 2, 2};
    Marking marking_222_alt{2, 2, 2};

    REQUIRE(!(marking_000 < marking_000));
    REQUIRE(marking_000 < marking_001);
    REQUIRE(marking_000 < marking_010);
    REQUIRE(marking_000 < marking_100);
    REQUIRE(marking_000 < marking_110);
    REQUIRE(marking_000 < marking_101);
    REQUIRE(marking_000 < marking_111);

    REQUIRE(marking_100 < marking_200);
    REQUIRE(marking_110 < marking_120);
    REQUIRE(marking_111 < marking_222);

    REQUIRE(!(marking_120 < marking_120));
    REQUIRE(!(marking_222 < marking_222_alt));

    REQUIRE(!(marking_001 < marking_000));
    REQUIRE(!(marking_010 < marking_000));
    REQUIRE(!(marking_100 < marking_000));
    REQUIRE(!(marking_110 < marking_000));
    REQUIRE(!(marking_101 < marking_000));
    REQUIRE(!(marking_111 < marking_000));

    REQUIRE(!(marking_200 < marking_100));
    REQUIRE(!(marking_120 < marking_110));
    REQUIRE(!(marking_222 < marking_111));

    REQUIRE(!(marking_100 < marking_010));
    REQUIRE(!(marking_010 < marking_100));
    REQUIRE(!(marking_010 < marking_001));
    REQUIRE(!(marking_001 < marking_010));
    REQUIRE(!(marking_120 < marking_200));
    REQUIRE(!(marking_200 < marking_120));
  }
}

TEST_CASE("Marking::operator[]", "[marking]") {
  SECTION("Markings of size 3") {
    SECTION("Non-const case") {
      Marking marking_012{0, 1, 2};

      REQUIRE(marking_012[0] == 0);
      REQUIRE(marking_012[1] == 1);
      REQUIRE(marking_012[2] == 2);
    }

    SECTION("Const case") {
      const Marking marking_012{0, 1, 2};

      REQUIRE(marking_012[0] == 0);
      REQUIRE(marking_012[1] == 1);
      REQUIRE(marking_012[2] == 2);
    }
  }
}


TEST_CASE("Marking::size()", "[marking]") {
  SECTION("Size 0 with default constructor") {
    Marking marking;

    REQUIRE(marking.size() == 0);
  }

  SECTION("Size 0") {
    Marking marking{};

    REQUIRE(marking.size() == 0);
  }

  SECTION("Size 1") {
    Marking marking{0};

    REQUIRE(marking.size() == 1);
  }
  SECTION("Size 2") {
    Marking marking{0, 0};

    REQUIRE(marking.size() == 2);
  }

  SECTION("Size 3") {
    Marking marking{0, 0, 0};

    REQUIRE(marking.size() == 3);
  }
}

TEST_CASE("Marking::add_omegaless_marking()", "[marking]") {
  SECTION("Markings of size 3") {
    Marking mapping_diff{1, -2, 3};

    SECTION("Example 1") {
      Marking marking{1, 2, 3};
      Marking expected_result{2, 0, 6};
      marking.add_omegaless_marking(mapping_diff);

      REQUIRE(marking == expected_result);
    }

    SECTION("Example 2") {
      Marking marking{Marking::omega, Marking::omega, 3};
      Marking expected_result{Marking::omega, Marking::omega, 6};
      marking.add_omegaless_marking(mapping_diff);

      REQUIRE(marking == expected_result);
    }
  }
}

TEST_CASE("Marking::accelerate_helper()", "[marking]") {
  SECTION("Markings of size 3") {
    const Marking ancestor_marking{0, 1, 2};

    SECTION("Example 1") {
      Marking marking{1, 1, 2};
      Marking expected_result{Marking::omega, 1, 2};
      marking.accelerate_helper(ancestor_marking);
      REQUIRE(marking == expected_result);
    }

    SECTION("Example 2") {
      Marking marking{2, 3, 2};
      Marking expected_result{Marking::omega, Marking::omega, 2};
      marking.accelerate_helper(ancestor_marking);
      REQUIRE(marking == expected_result);
    }

    SECTION("Example 3") {
      Marking marking{6, 2, 5};
      Marking expected_result{Marking::omega, Marking::omega, Marking::omega};
      marking.accelerate_helper(ancestor_marking);
      REQUIRE(marking == expected_result);
    }

    SECTION("Example 4") {
      Marking marking{0, 1, 2};
      Marking expected_result{0, 1, 2};
      marking.accelerate_helper(ancestor_marking);
      REQUIRE(marking == expected_result);
    }
  }
}

TEST_CASE("operator<< for Marking", "[marking]") {
  Marking marking;
  std::stringstream stream;
  stream << marking;
  std::string ref("");
  REQUIRE(stream.str() == ref);

  marking = Marking(1);
  stream.clear();
  stream.str("");
  stream << marking;
  ref = "0";
  REQUIRE(stream.str() == ref);

  marking = Marking(2, 3);
  stream.clear();
  stream.str("");
  stream << marking;
  ref = "3 3";
  REQUIRE(stream.str() == ref);

  marking = Marking{1, 2, 3};
  stream.clear();
  stream.str("");
  stream << marking;
  ref = "1 2 3";
  REQUIRE(stream.str() == ref);
}

#endif  // COMPILE_TESTS
